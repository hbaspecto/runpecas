runpecas
==================================================

This is the main run program for PECAS.

LICENSE
=======

Copyright 2021 HBA Specto Incorporated

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

OVERVIEW
========

This repository is closely integrated with two other
repositories: ``pecas-routines`` and ``hba-util``. In general,
the purpose of each of these is:

- The ``runpecas`` repository is for sequencing and monitoring
  the steps needed to run PECAS. This includes the main run program,
  the subsidiary run modules for each module, and run tasks like
  database backup and restore.
- The ``pecas-routines`` repository is part of the PECAS model,
  along with the AA and SD Java programs. It includes routines
  that process model data to send from one module to another,
  such as AA-SD price and floor space corrections, SD phasing,
  employment, and skim squeezing.
- The ``hba-util`` repository is for utility modules that are
  useful both for PECAS itself and for related tools such as
  the parcel cutter. It includes libraries for handling settings,
  interacting with databases, and loading to MapIt.

A key difference between the repositories is *stability*. Changes
to ``runpecas`` and ``hba-util`` generally add new features or fix
bugs without changing existing functionality, so upgrading these
modules is less likely to change model results.

In contrast, great
caution must be used when upgrading ``pecas-routines`` in an
existing scenario, since any changes to these modules are likely
to be improvements to model behaviour (e.g. to make it more realistic
or stable), making it impossible to reproduce the scenario results.

For this reason, make targets are provided to upgrade ``runpecas``
alone (``_install_runpecas_only``) and to upgrade ``hba-util`` alone
(``_install_hbautil_only``). The only way to upgrade ``pecas-routines``
is to do a full reinstall, which is not recommended for existing
scenarios.

This repository also stores the canonical versions of
``aa.properties`` and ``sd.properties``, in the
``scenario/AllYears/Inputs`` folder.

Links:

- BB:      https://bitbucket.org/hbaspecto/runpecas
- Docs:    http://docs.office.hbaspecto.com/runpecas
- Jenkins: http://jenkins-2.office.hbaspecto.com/job/runpecas

Notes:

- ``runpecas`` replaces `hba-python <https://bitbucket.org/hbaspecto/hba-python>`_.


Quickstart / Testing
--------------------------------------------------

::

    git clone git@bitbucket.org:hbaspecto/runpecas.git

    cd runpecas
    source ./hba-setup.env

    # Create the virtual environment.
    make _ve_build

    # Run the tests.
    make _test


Setting up a PECAS scenario to auto-install runpecas
----------------------------------------------------

You will need Git and an SSH key for the Bitbucket repository.

On Linux:

- Copy ``scenario/Makefile`` and ``scenario/Makefile.inc`` into the scenario root.

  - You can add custom make targets for a scenario to ``Makefile``. Leave ``Makefile.inc`` alone.

- If the scenario already has a version of runpecas specified and you want to install it in
  the run director, run ``make _install``
- To upgrade to the latest version of runpecas or do a brand new install, run ``make _install_latest_from_repository``.
  DO NOT do this if you've already run the scenario, as it may change scenario results!
- To use a specific version of runpecas:

  - If there is no ``repositories.yml``, ``make _init``.
  - Edit the ``repositories.yml`` file, replacing the default git hashes with the ones you need.
  - Run ``make _install``.

- Create the local machine settings file, which should be called ``machine_<name>.yml``,
  where ``<name>`` is the lowercased hostname up to the first dot (e.g. ``server`` if
  the machine is "server.example.com").
- Configure ``pecas.yml``.
- If necessary, add project-specific implementations to ``project_code.py``.
- Activate the commands using ``source ve/bin/activate``.
- Start the run using ``runpecas``.

Runpecas accepts several options:

- ``--resume`` (to resume from where the run stopped)
- ``--norestore`` (to skip restoring the database from the filesystem)
- ``--notech`` (to skip technology scaling)
- ``--notemp`` (to skip creating the SD temporary table)

The following commands can also be run once commands have been activated:

- ``sd_restore`` and ``sd_backup`` load and save the scenario database, respectively.
- ``load_output`` manually loads model outputs to MapIt, in case automatic MapIt
  uploads were turned off or failed.
- The ``migratesdX_X`` commands upgrade older versions of the scenario database.
  Currently, the available upgrade commands are ``migratesd2_9``, ``migratesd2_10``,
  and ``migratesd2_11``.
- ``input_views`` and ``output_views`` create geographic views that can be
  imported into QGIS for viewing and analysis.


On Windows:

Git for Windows helpfully comes with a bash implementation, Git Bash.
Make note of the install path (usually something like ``C:\Program Files\Git\bin\sh.exe``).
You may need to change this in the ``winShCommand`` setting in ``machine_<machine-name>.yml``.

- Install a Windows port of the ``make`` utility,
  e.g. get from here `from here <https://sourceforge.net/projects/ezwinports/files>`_
  (download the version that looks like ``make-VERSION-without-guile-w32-bin.zip``),
  extract the files, and copy them into the ``Git\mingw64`` folder.
- Make sure the Python install you want to use is in the PATH environment variable,
  and earlier than all the other Python installs.
- Open a Git Bash window in the scenario root. This should be available in the context
  menu that pops up when you right-click in the background of the Explorer window.
- Follow the Linux instructions, typing the commands into the Git Bash window, except
  that the VE activation command is
  ``source ve-win/Scripts/activate`` (``ve-win`` instead of ``ve`` and ``Scripts`` instead of ``bin``).

Running ad hoc Python
----------------------

Sometimes it's useful to test components of the PECAS system by running individual
Python functions in the Python interpreter. To do this::

    source ve/bin/activate
    python3
    >>> from hbautil import pecassetup
    >>> ps = pecassetup.load_pecas_settings()

Then enter the PECAS functions you wish to run.

The ``source ve/bin/activate`` command ensures that the Python interpreter you
start has access to all the PECAS modules. Then you need to load the PECAS
settings (from pecas.yml and machine.yml) using ``pecassetup.load_pecas_settings()``;
the settings object must be passed as the first argument to most PECAS functions.

If you need to overwrite settings, use the ``ps.clone_with()`` method;
for example::

    >>> ps = ps.clone_with(startyear=2020, stopyear=2030, load_output_to_mapit=False)

