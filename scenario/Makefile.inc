#
# This file has been copied from the runpecas repository
# (https://bitbucket.org/hbaspecto/runpecas/src/master/scenario/Makefile)
# Any permanent changes you make here should be copied there as well.
#

SHELL:=bash

repos = repositories.yml
tdm_client_repo = repositories-tdm-client.yml
calib_repo = repositories-calib.yml
git_url = https://bitbucket.org/hbaspecto
git_pip = git+${git_url}
git_auth_url = https://${GIT_USER}:${GIT_APP_PASSWORD}@bitbucket.org/hbaspecto
git_auth_pip = git+${git_auth_url}

_debug:
	echo ${git_pip}

latest_hash = $(word 1, $(shell git ls-remote ${git_url}/${1}.git HEAD))
latest_hash_auth = $(word 1, $(shell git ls-remote ${git_auth_url}/${1}.git HEAD))

latest_runpecas_hash = $(call latest_hash,runpecas)
latest_pecas_routines_hash = $(call latest_hash,pecas-routines)
latest_pecas_techscaling_hash = $(call latest_hash,pecas_technology_scaling)
latest_hbautil_hash = $(call latest_hash,hba-util)
latest_tdm_client_hash = $(call latest_hash,hba-tdm-server)
latest_calib_hash = $(call latest_hash_auth,pecas-calib)

define DEFAULT_REPOSITORIES
runpecas_version: ${latest_runpecas_hash}
pecas_routines_version: ${latest_pecas_routines_hash}
pecas_technology_scaling_version: ${latest_pecas_techscaling_hash}
hbautil_version: ${latest_hbautil_hash}
endef

export DEFAULT_REPOSITORIES

#####

# Targets starting with '_mrsgui' are the interface with Mrsgui.
# "mrsgui_core.ctx.MrsguiCtx:job_start()" uses them.
# https://bitbucket.org/hbaspecto/mrsgui/src/master/mrsgui-core/mrsgui_core/ctx.py
_mrsgui_install:
	make _install
	@if [ -f ${runpecas_dir}/mrsgui-commands.yml ]; then \
		cp ${runpecas_dir}/mrsgui-commands.yml ${ve_dir}/mrsgui-commands.yml; \
	fi

#####

_install_latest_from_repository:
	make _init
	make _install

_init:
	@echo "$$DEFAULT_REPOSITORIES" > ${repos}

_install:
	make _ve_rebuild
	make _user_files

_install_runpecas_only:
	${pip3_cmd} install --force-reinstall -e ${git_pip}/runpecas.git@${runpecas_hash}#egg=runpecas

_install_hbautil_only:
	${pip3_cmd} install --force-reinstall ${git_pip}/hba-util.git@${hbautil_hash}#egg=hbautil

_install_tdm_client:
	${pip3_cmd} install --force-reinstall ${git_pip}/hba-tdm-server.git@${tdm_client_hash}#egg=hba-tdm-server

_install_calib:
	${pip3_cmd} install --force-reinstall ${git_auth_pip}/pecas-calib.git@${calib_hash}#egg=pecas-calib
	${ve_dir}/${bin_dir}/calib_user_files;

_install_analysis:
	${pip3_cmd} install --force-reinstall -e ${git_pip}/pecas-analysis.git#egg=analysis

_install_parcel_cut:
	${pip3_cmd} install --force-reinstall ${git_pip}/parcelcut.git#egg=parcelcut

_install_editable_gis_views:
	${pip3_cmd} install --force-reinstall ${git_auth_pip}/editable-gis-views.git#egg=editable-gis-views
	${ve_dir}/${bin_dir}/views_settings
	${ve_dir}/${bin_dir}/inject_qgis

setupgrade = $(shell sed 's/${1}_version:[[:space:]]*${2}/${1}_version: ${3}/' ${repos} > ${repos}.new && mv ${repos}.new ${repos})

_set_tdm_client_version:
	echo "tdm_client_version: ${latest_tdm_client_hash}" > ${tdm_client_repo}

_set_calib_version:
	echo "calib_version: ${latest_calib_hash}" > ${calib_repo}

_upgrade_runpecas:
	$(call setupgrade,runpecas,${runpecas_hash},${latest_runpecas_hash})
	make _install_runpecas_only

_upgrade_hbautil:
	$(call setupgrade,hbautil,${hbautil_hash},${latest_hbautil_hash})
	make _install_hbautil_only

_upgrade_tdm_client:
	make _set_tdm_client_version
	make _install_tdm_client

_upgrade_calib:
	make _set_calib_version
	make _install_calib

#####

ifdef COMSPEC
	# Windows configuration
    python_name := python
	bin_dir := Scripts
	ve_dir := ve-win
	sys_python3_exe=$(shell which ${python_name})
else
    python_name := python3
	bin_dir := bin
	ve_dir := ve
	sys_python3_exe=$(shell PATH=/usr/bin:/usr/local/bin:${PATH} type -p python3)
endif

pip3_cmd=${ve_dir}/${bin_dir}/${python_name} -m pip
runpecas_dir=${ve_dir}/src/runpecas/runpecas

# do we have a python3?
ifeq (${sys_python3_exe},)
  $(error "No sys_python3_exe found")
endif

load_hash = $(strip $(shell grep ${1} ${repos} | cut -d : -f 2))
load_hash_from = $(strip $(shell head -n 1 ${1} | cut -d : -f 2))

runpecas_hash = $(call load_hash,runpecas_version)
pecas_routines_hash = $(call load_hash,pecas_routines_version)
pecas_techscaling_hash = $(call load_hash,pecas_technology_scaling_version)
hbautil_hash = $(call load_hash,hbautil_version)
tdm_client_hash = $(call load_hash_from,${tdm_client_repo})
calib_hash = $(call load_hash_from,${calib_repo})


_ve_build:
	mkdir -p "$(dir ${ve_dir})"
	"${sys_python3_exe}" -m venv "${ve_dir}"
	${pip3_cmd} install --upgrade pip
	make _install_runpecas_only
	${pip3_cmd} install ${git_pip}/pecas-routines.git@${pecas_routines_hash}#egg=pecas_routines
	${pip3_cmd} install ${git_pip}/pecas_technology_scaling@${pecas_techscaling_hash}#egg=techscaling
	make _install_hbautil_only
	@if [ -f ${tdm_client_repo} ]; then make _install_tdm_client; fi
	@if [ -f ${calib_repo} ]; then make _install_calib; fi


_ve_rm:
	-rm -rf "${ve_dir}"

_ve_rebuild: _ve_rm _ve_build

# We put user file commands in the user_files program now, otherwise use the old hardcoded commands
_user_files:
	@if [ -f ${ve_dir}/${bin_dir}/user_files ]; then \
		${ve_dir}/${bin_dir}/user_files ${ve_dir}; \
	else \
		${ve_dir}/${bin_dir}/update_settings ${runpecas_dir}/pecas.yml pecas.yml --old-python pecas_settings.py; \
		${ve_dir}/${bin_dir}/update_settings ${runpecas_dir}/machine.yml machine.yml --old-python machine_settings.py; \
		cp -n ${runpecas_dir}/project_code_template.py project_code.py || true; \
	fi
	@if [ -f ${calib_repo} ]; then \
		${ve_dir}/${bin_dir}/calib_user_files; \
	fi
