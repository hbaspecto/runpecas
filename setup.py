from setuptools import setup

setup(
    name="runpecas",
    version="1.0",
    packages=["runpecas"],
    package_data={
        "": [
            "*.sh",
            "*.yml",
            ],
    },
    install_requires=[
        "setuptools-git",
    ],
    entry_points={
        "console_scripts": [
            "runpecas = runpecas.runpecas:parse_arguments_and_run",
            "run_aa = runpecas.run_aa:get_args_and_run",
            "run_popsyn = runpecas.run_popsyn:main",
            "sd_backup = runpecas.sd_backup:main",
            "sd_restore = runpecas.sd_restore:main",
            "load_output = runpecas.load_output:main",
            "update_settings = runpecas.update_settings:parse_arguments_and_run",
            "user_files = runpecas.user_files:main",
            "input_views = runpecas.input_views:run_default",
            "output_views = runpecas.output_views:run_default",
            "run_sd = runpecas.run_sd:get_args_and_run",
        ]
    }
)
