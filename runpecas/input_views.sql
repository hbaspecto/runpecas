-- START Zoning category type

-- drop materialized view if exists {sch}.zoning_category_type;
create materialized view if not exists {sch}.zoning_category_type as
select a.*, b.max_intensities from
(
 SELECT DISTINCT ON (sub.zoning_rules_code) sub.zoning_rules_code,
     sub.zoning_rules_code_name,
    sub.space_category,
    sub.max_intensity_permitted
   FROM ( SELECT zri.zoning_rules_code,
		 zri.zoning_rules_code_name,
        zp_1.space_type_id as max_intensity_space_type_id,
        sc.space_category,
        zp_1.max_intensity_permitted
       FROM {sch}.zoning_rules_i zri
		 left join {sch}.zoning_permissions zp_1 using (zoning_rules_code)
         left JOIN {sch}.space_categories sc USING (space_type_id)
         ) sub
  ORDER BY sub.zoning_rules_code, sub.max_intensity_permitted DESC, max_intensity_space_type_id
) a
left join
(SELECT zoning_rules_code, string_agg(space_type_name || ':' || max_intensity_permitted, ', ' order by space_type_id) as max_intensities
        FROM {sch}.zoning_permissions
        join {sch}.space_types_i using (space_type_id)
        group by zoning_rules_code
        order by zoning_rules_code) b
using (zoning_rules_code);

refresh  materialized view {sch}.zoning_category_type;

-- END Zoning category type

-- START Base year space

create sequence if not exists {sch}.pecas_parcel_num_seq;

SELECT setval('{sch}.pecas_parcel_num_seq', max(pecas_parcel_num)) FROM {sch}.parcels_backup_with_geom;

CREATE OR REPLACE FUNCTION drop_any_type_of_view_if_exists(IN _schemaname text, IN _viewname text)
RETURNS VOID AS
$$
BEGIN
    RAISE LOG 'Looking for (materialized) view named %%', _viewname;
    IF EXISTS (SELECT matviewname from pg_matviews where schemaname = _schemaname and matviewname = _viewname) THEN
        RAISE NOTICE 'DROP MATERIALIZED VIEW %%', _viewname;
        EXECUTE 'DROP MATERIALIZED VIEW ' || _schemaname || '.' || quote_ident(_viewname);
    ELSEIF EXISTS (SELECT viewname from pg_views where schemaname = _schemaname and viewname = _viewname) THEN
        RAISE NOTICE 'DROP VIEW %%', _viewname;
        EXECUTE 'DROP VIEW ' || _schemaname || '.' || quote_ident(_viewname);
    ELSE
        RAISE NOTICE 'NO VIEW %% found', _viewname;
    END IF;
END;
$$ LANGUAGE plpgsql;

select drop_any_type_of_view_if_exists('{sch}', 'base_parcels_with_geom');

create view {sch}.base_parcels_with_geom as
select par.parcel_id,
  par.pecas_parcel_num,
  par.year_built,
  par.space_type_id,
  st.space_type_name,
  par.space_quantity,
  par.land_area,
  par.available_services_code,
  par.is_derelict,
  par.is_brownfield,
  par.taz,
  par.geom
from {sch}.parcels_backup_with_geom par
join {sch}.space_types_i st using (space_type_id);

CREATE or replace FUNCTION {sch}.update_base_parcel()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
declare
	new_taz integer;
BEGIN

 IF (TG_OP = 'UPDATE') THEN

	-- TODO check to make sure geometry hasn't changed

	select taz_number into new_taz from {sch}.tazs t
	where st_contains(t.geom, st_pointonsurface(new.geom));
	update {sch}.parcels_backup_with_geom set
	   parcel_id = NEW.parcel_id,
	   year_built = NEW.year_built,
    	space_type_id = NEW.space_type_id,
    	space_quantity = NEW.space_quantity,
    	land_area = st_area(NEW.geom),
    	available_services_code = NEW.available_services_code,
	    is_derelict = NEW.is_derelict,
    	is_brownfield = NEW.is_brownfield,
    	geom = NEW.geom,
    	taz = new_taz
	where  pecas_parcel_num = OLD.pecas_parcel_num;
	update {sch}.parcels set
	   parcel_id = NEW.parcel_id,
	   year_built = NEW.year_built,
    	space_type_id = NEW.space_type_id,
    	space_quantity = NEW.space_quantity,
    	land_area = st_area(NEW.geom),
    	available_services_code = NEW.available_services_code,
	    is_derelict = NEW.is_derelict,
    	is_brownfield = NEW.is_brownfield,
    	taz = new_taz
	where pecas_parcel_num = OLD.pecas_parcel_num;
	RETURN NEW;
 elseif (TG_OP = 'INSERT') THEN
	select taz_number into new_taz from {sch}.tazs t
	where st_contains(t.geom,st_pointonsurface(new.geom));
	INSERT INTO {sch}.parcels_backup_with_geom(
	parcel_id, pecas_parcel_num, year_built, taz, space_type_id, space_quantity, land_area, available_services_code, is_derelict, is_brownfield, geom)
	VALUES (new.parcel_id, nextval('{sch}.pecas_parcel_num_seq'),
	   new.year_built, new_taz, new.space_type_id, new.space_quantity,
	   st_area(new.geom), new.available_services_code, new.is_derelict, new.is_brownfield, new.geom);
	INSERT INTO {sch}.parcels(
	parcel_id, pecas_parcel_num, year_built, taz, space_type_id, space_quantity, land_area, available_services_code, is_derelict, is_brownfield)
	VALUES (new.parcel_id, currval('{sch}.pecas_parcel_num_seq'),
	   new.year_built, new_taz, new.space_type_id, new.space_quantity,
	   st_area(new.geom), new.available_services_code, new.is_derelict, new.is_brownfield);
    RETURN NEW;
 elseif (TG_OP = 'DELETE') then
 	delete from {sch}.parcels_backup_with_geom where pecas_parcel_num = old.pecas_parcel_num;
 	delete from {sch}.parcels where pecas_parcel_num = old.pecas_parcel_num;
	RETURN OLD;
 end if;
END;
$BODY$;


CREATE TRIGGER update_pecas_parcel_geom
    INSTEAD OF UPDATE or INSERT or DELETE
    ON {sch}.base_parcels_with_geom
    FOR EACH ROW
    EXECUTE PROCEDURE {sch}.update_base_parcel();

-- END Base year space

-- START Earliest Zoning

drop materialized view if exists {sch}.earliest_zoning cascade;

create materialized view {sch}.earliest_zoning as
 select par.pecas_parcel_num,
    zp.zoning_rules_code,
    zp.zoning_rules_code_name,
    zp.space_category,
    zp.max_intensity_permitted,
    zp.max_intensities,
    coalesce(ph.initial_release_year, zx.year_effective) as release_year,
    px.plan_id,
    px.phase_number,
    par.geom
   from {sch}.parcels_backup_with_geom par
     left join ( select distinct on (zx_1.pecas_parcel_num) zx_1.pecas_parcel_num,
            zx_1.zoning_rules_code,
            zx_1.year_effective
           from {sch}.parcel_zoning_xref zx_1
           order by zx_1.pecas_parcel_num, zx_1.year_effective) zx using (pecas_parcel_num)
     left join {sch}.phasing_plan_xref px using (pecas_parcel_num)
     left join {sch}.phasing_plans ph using (plan_id)
     left join {sch}.zoning_category_type zp
        on coalesce(px.zoning_rules_code, zx.zoning_rules_code) = zp.zoning_rules_code
;

create unique index on {sch}.earliest_zoning (pecas_parcel_num);
create index on {sch}.earliest_zoning using gist(geom);

-- END Earliest Zoning

-- START All Zoning

drop view if exists {sch}.zoning_xref_geom cascade;
drop materialized view if exists {sch}.zoning_xref_geom cascade;
drop table if exists {sch}.zoning_xref_uq_id cascade;

--- create the table that stooopid QGIS needs because it can't deal with multiple field primary keys
drop sequence if exists {sch}.zoning_seq cascade;
create sequence {sch}.zoning_seq;

drop table if exists {sch}.zoning_xref_uq_id cascade;
CREATE table {sch}.zoning_xref_uq_id
as select nextval('{sch}.zoning_seq') as id, pecas_parcel_num, year_effective
from {sch}.parcel_zoning_xref;

alter table {sch}.zoning_xref_uq_id add primary key (id);
create unique index on {sch}.zoning_xref_uq_id (pecas_parcel_num, year_effective);

-- drop view public.zoning_xref_geom;
create view {sch}.zoning_xref_geom
AS
 select uq.id,
    parcel_zoning_xref.pecas_parcel_num,
    parcel_zoning_xref.zoning_rules_code,
    zp.zoning_rules_code_name,
    parcel_zoning_xref.year_effective,
    zp.space_category as max_intensity_space_category,
    zp.max_intensity_permitted,
	zp.max_intensities,
    parcels_backup_with_geom.geom
   from {sch}.parcel_zoning_xref
     JOIN {sch}.parcels_backup_with_geom using (pecas_parcel_num)
	 join {sch}.zoning_xref_uq_id uq using (pecas_parcel_num, year_effective)
     left join {sch}.zoning_category_type zp using (zoning_rules_code)
;



CREATE or replace FUNCTION {sch}.update_zoning()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
 IF (TG_OP = 'UPDATE') THEN

	-- TODO check to make sure geometry hasn't changed
	update {sch}.parcel_zoning_xref set
		year_effective = NEW.year_effective,
		zoning_rules_code = NEW.zoning_rules_code
	where  pecas_parcel_num = OLD.pecas_parcel_num and year_effective = OLD.year_effective;
	update {sch}.zoning_xref_uq_id set
		year_effective = NEW.year_effective
	where id = OLD.id;
	RETURN NEW;
 elseif (TG_OP = 'INSERT') THEN
 	-- TODO check to make sure not a duplicate and that the pecas_parcel_num exists
 	-- TODO also check to make sure the geometry hasn't changed
 	insert into {sch}.parcel_zoning_xref (pecas_parcel_num, year_effective, zoning_rules_code) values
		(NEW.pecas_parcel_num, NEW.year_effective, NEW.zoning_rules_code);
	insert into {sch}.zoning_xref_uq_id (id,pecas_parcel_num, year_effective) values
		(nextval('{sch}.zoning_seq'), NEW.pecas_parcel_num, NEW.year_effective);
	RETURN NEW;
 elseif (TG_OP = 'DELETE') then
 	delete from {sch}.parcel_zoning_xref where year_effective = OLD.year_effective and pecas_parcel_num = OLD.pecas_parcel_num;
	delete from {sch}.zoning_xref_uq_id where year_effective = OLD.year_effective and pecas_parcel_num = OLD.pecas_parcel_num;
	RETURN OLD;
 end if;
END;
$BODY$;

CREATE TRIGGER update_zoning
    INSTEAD OF UPDATE or INSERT or DELETE
    ON {sch}.zoning_xref_geom
    FOR EACH ROW
    EXECUTE PROCEDURE {sch}.update_zoning();

-- END All Zoning

-- START All Phasing

drop view if exists {sch}.phasing_plan_xref_geom;

DROP FUNCTION IF EXISTS {sch}.update_phasing();

CREATE OR REPLACE VIEW {sch}.phasing_plan_xref_geom
 AS
 select phasing_plan_xref.pecas_parcel_num,
    phasing_plan_xref.plan_id,
    phasing_plan_xref.phase_number,
    phasing_plan_xref.zoning_rules_code,
    zp.zoning_rules_code_name,
    zp.space_category as max_intensity_space_category,
    zp.max_intensity_permitted,
    zp.max_intensities,
    parcels_backup_with_geom.geom
   from {sch}.phasing_plan_xref
     JOIN {sch}.parcels_backup_with_geom using (pecas_parcel_num)
     left join {sch}.zoning_category_type zp using (zoning_rules_code)
;

ALTER TABLE {sch}.phasing_plan_xref_geom
    OWNER TO postgres;

CREATE FUNCTION {sch}.update_phasing()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
	 IF (TG_OP = 'UPDATE') THEN
	-- TODO check to make sure geometry hasn't changed
		update {sch}.phasing_plan_xref set
			plan_id = NEW.plan_id,
			phase_number = NEW.phase_number,
			zoning_rules_code = NEW.zoning_rules_code
		where pecas_parcel_num = OLD.pecas_parcel_num;
		RETURN NEW;
	elseif (TG_OP = 'DELETE') THEN
		delete from {sch}.phasing_plan_xref where pecas_parcel_num = OLD.pecas_parcel_num;
		return OLD;
	elseif (TG_OP = 'INSERT') THEN
	-- TODO should check that parcel does exist and that geom hasn't changed and that it's not going to do a unqiue key error
		insert into {sch}.phasing_plan_xref (pecas_parcel_num, plan_id, phase_number, zoning_rules_code) VALUES
		(NEW.pecas_parcel_num, NEW.plan_id, NEW.phase_number, NEW.zoning_rules_code);
		return NEW;
	end if;
END;
$BODY$;

CREATE TRIGGER update_phasing
    INSTEAD OF UPDATE OR INSERT OR DELETE
    ON {sch}.phasing_plan_xref_geom
    FOR EACH ROW
    EXECUTE PROCEDURE {sch}.update_phasing();

-- END All Phasing

-- START Costs

drop materialized view if exists {sch}.base_costs;

create materialized view {sch}.base_costs as

with cost_by_parcel as
(
	select distinct on (pecas_parcel_num) * from
	(
		select cx.*, tc.space_type_id, sc.space_category, tc.construction_cost
		from {sch}.parcel_cost_xref cx
		join {sch}.earliest_zoning zx using (pecas_parcel_num)
		join {sch}.zoning_permissions zp using (zoning_rules_code)
		join {sch}.transition_costs tc
			on tc.cost_schedule_id = cx.cost_schedule_id and tc.space_type_id = zp.space_type_id
		join {sch}.space_categories sc
			on sc.space_type_id = tc.space_type_id
		where cx.year_effective <= %(baseyear)s
	) sub
	order by pecas_parcel_num, construction_cost, space_type_id
)

select par.pecas_parcel_num, cx.cost_schedule_id,
cx.space_category,
cx.construction_cost,
cx.construction_cost - mcx.most_common_construction_cost as relative_cost,
par.geom
from {sch}.parcels_backup_with_geom par
join cost_by_parcel cx using (pecas_parcel_num)
join (
	select space_type_id, construction_cost as most_common_construction_cost from
	(
		select space_type_id, construction_cost,
		rank() over (partition by space_type_id order by freq desc) as rn
		from
		(
			select space_type_id, construction_cost, count(*) as freq
			from cost_by_parcel
			group by space_type_id, construction_cost
		) sub
	) sub
	where rn = 1
) mcx on mcx.space_type_id = cx.space_type_id;

create unique index on {sch}.base_costs (pecas_parcel_num);
create index on {sch}.base_costs using gist(geom);

-- END Costs

-- START Fees

drop materialized view if exists {sch}.base_fees;

create materialized view {sch}.base_fees as

with fee_by_parcel as
(
	select distinct on (pecas_parcel_num) * from
	(
		select fx.*, df.space_type_id, sc.space_category,
		df.development_fee_per_unit_space_initial +
			df.development_fee_per_unit_space_ongoing / %(amortization)s
			as fee_per_space,
		df.development_fee_per_unit_land_initial +
			df.development_fee_per_unit_land_ongoing / %(amortization)s
			as fee_per_land
		from {sch}.parcel_fee_xref fx
		join {sch}.earliest_zoning zx using (pecas_parcel_num)
		join {sch}.zoning_permissions zp using (zoning_rules_code)
		join {sch}.development_fees df
			on df.fee_schedule_id = fx.fee_schedule_id and df.space_type_id = zp.space_type_id
		join {sch}.space_categories sc
			on sc.space_type_id = df.space_type_id
		where fx.year_effective <= %(baseyear)s
	) sub
	order by pecas_parcel_num, fee_per_space, fee_per_land, space_type_id
)

select par.pecas_parcel_num, fx.fee_schedule_id,
fx.space_category,
fx.fee_per_space,
fx.fee_per_land,
fx.fee_per_land - mfx.most_common_fee_per_land as relative_fee_per_land,
par.geom
from {sch}.parcels_backup_with_geom par
join fee_by_parcel fx using (pecas_parcel_num)
join (
	select space_type_id, fee_per_land as most_common_fee_per_land from
	(
		select space_type_id, fee_per_land,
		rank() over (partition by space_type_id order by freq desc) as rn
		from
		(
			select space_type_id, fee_per_land, count(*) as freq
			from fee_by_parcel
			group by space_type_id, fee_per_land
		) sub
	) sub
	where rn = 1
) mfx on mfx.space_type_id = fx.space_type_id;

create unique index on {sch}.base_fees (pecas_parcel_num);
create index on {sch}.base_fees using gist(geom);

-- END Fees

-- START Local effects

drop materialized view if exists {sch}.base_local_effect_distances;

create materialized view {sch}.base_local_effect_distances as
select par.pecas_parcel_num, led.local_effect_id, led.local_effect_name,
led.local_effect_distance as distance,
par.geom
from {sch}.parcels_backup_with_geom par
join (
	select led.*, le.local_effect_name
	from {sch}.local_effect_distances led
	join {sch}.local_effects le
	using (local_effect_id)
	where year_effective <= %(baseyear)s
) led using (pecas_parcel_num);

create unique index on {sch}.base_local_effect_distances (pecas_parcel_num, local_effect_id);
create index on {sch}.base_local_effect_distances using gist(geom);

-- END Local effects

-- START Site spec

select drop_any_type_of_view_if_exists('{sch}', 'sitespec_parcels_with_geom');

CREATE OR REPLACE VIEW {sch}.sitespec_parcels_with_geom
 AS
 SELECT par.pecas_parcel_num,
    ss.space_type_id,
    st.space_type_name,
    sc.space_category,
    ss.space_quantity,
    ss.space_quantity / ss.land_area AS far,
    ss.year_effective,
    par.geom
   FROM {sch}.parcels_backup_with_geom par
     JOIN {sch}.sitespec_parcels ss
     USING (pecas_parcel_num)
     JOIN {sch}.space_types_i st ON st.space_type_id = ss.space_type_id
     left JOIN {sch}.space_categories sc ON sc.space_type_id = ss.space_type_id
  WHERE ss.update_parcel;

-- View: {sch}.sitespec_parcels_with_geom


drop table if exists {sch}.sitespec_xref_uq_id cascade;

--- create the table that stooopid QGIS needs because it can't deal with multiple field primary keys
drop sequence if exists {sch}.sitespec_seq cascade;
create sequence {sch}.sitespec_seq;

CREATE table {sch}.sitespec_xref_uq_id
as select nextval('{sch}.sitespec_seq') as id, pecas_parcel_num, year_effective
from {sch}.sitespec_parcels;

alter table {sch}.sitespec_xref_uq_id add primary key (id);
create unique index on {sch}.sitespec_xref_uq_id (pecas_parcel_num, year_effective);

DROP  VIEW if exists {sch}.all_sitespec_parcels_with_geom;

CREATE or replace VIEW {sch}.all_sitespec_parcels_with_geom
AS
 SELECT
 uq.id,
 par.pecas_parcel_num,
 ss.siteid,
 ss.space_type_id,
 ss.space_quantity,
 --ss.land_area,
 ss.year_effective,
 ss.update_parcel,
 ss.space_quantity/ss.land_area as far,
 par.geom
   FROM {sch}.sitespec_parcels ss
     JOIN {sch}.parcels_backup_with_geom par
	 USING (pecas_parcel_num)
  join {sch}.sitespec_xref_uq_id uq using (pecas_parcel_num, year_effective);

-- FUNCTION: {sch}.update_zoning()

-- DROP FUNCTION {sch}.update_zoning();

CREATE or replace FUNCTION {sch}.update_sitespec_parcels()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
 IF (TG_OP = 'UPDATE') THEN
	-- TODO check to make sure geometry hasn't changed
	update {sch}.sitespec_parcels set
	siteid = NEW.siteid,
	space_type_id = NEW.space_type_id,
	space_quantity = NEW.space_quantity,
	land_area = st_area(NEW.geom),
	year_effective = NEW.year_effective,
	update_parcel = NEW.update_parcel
	where  pecas_parcel_num = OLD.pecas_parcel_num and year_effective = OLD.year_effective;
	update {sch}.sitespec_xref_uq_id set
		year_effective = NEW.year_effective
	where id = OLD.id;
	RETURN NEW;
 elseif (TG_OP = 'INSERT') THEN
 	-- TODO check to make sure not a duplicate and that the pecas_parcel_num exists
 	-- TODO also check to make sure the geometry hasn't changed
 	insert into {sch}.sitespec_parcels (pecas_parcel_num, siteid, space_type_id, space_quantity, land_area,
									   year_effective, update_parcel) values
		(NEW.pecas_parcel_num, NEW.siteid, NEW.space_type_id, NEW.space_quantity, st_area(NEW.geom), NEW.year_effective, NEW.update_parcel);
	insert into {sch}.sitespec_xref_uq_id (id,pecas_parcel_num, year_effective) values
		(nextval('{sch}.sitespec_seq'), NEW.pecas_parcel_num, NEW.year_effective);
		RETURN NEW;
 elseif (TG_OP = 'DELETE') then
 	delete from {sch}.sitespec_parcels where year_effective = OLD.year_effective and pecas_parcel_num = OLD.pecas_parcel_num;
	delete from {sch}.sitespec_xref_uq_id where year_effective = OLD.year_effective and pecas_parcel_num = OLD.pecas_parcel_num;
	RETURN OLD;
 end if;
END;
$BODY$;


CREATE TRIGGER update_sitespec
    INSTEAD OF INSERT OR DELETE OR UPDATE
    ON {sch}.all_sitespec_parcels_with_geom
    FOR EACH ROW
    EXECUTE PROCEDURE {sch}.update_sitespec_parcels();



-- END Site spec
