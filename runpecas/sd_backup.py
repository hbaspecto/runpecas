import argparse

import pecas_routines as pr
from hbautil import sd_backup_restore


def main():
    ps = pr.load_pecas_settings()

    parser = argparse.ArgumentParser(
        description="Saves the SD database to the filesystem."
    )
    parser.add_argument(
        "-s", "--schema", default=ps.sd_schema, help="The SD schema to back up; defaults to this scenario's schema"
    )

    args = parser.parse_args()

    pr.set_up_logging(ps)
    sd_backup_restore.backup_sd(
        ps,
        split=True, overwrite=True, use_environ=False, schema=args.schema, threaded=False
    )


if __name__ == "__main__":
    main()
