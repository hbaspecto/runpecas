#! /bin/bash

##################################################################################################
# HELPER FUNCTIONS
##################

function stripspace {
    echo -e $1 | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'
}

function getsystype {
    uname=$(uname)
    if [[ "${uname}" == "Darwin" ]]
    then
        echo "posix"
    elif [[ "${uname}" == "Linux" ]]
    then
        echo "posix"
    elif [[ "${uname}" == *"_NT-"* ]]
    then
        echo "windows"
    fi
}

function set_db {
    export PGHOST=$1
    export PGPORT=$2
    export PGUSER=$3
    export PGPASSWORD=$4
    export PGDATABASE=$5
}

function get_machinesetting {
    if [[ "$1" == "str" ]]
    then
        echo $(stripspace $(grep $2 machine_settings.py | cut -d= -f2 | tr -d \' | tr -d \"))
    else
        echo $(stripspace $(grep $1 machine_settings.py | cut -d= -f2 ))
    fi
}

function rsync_cmd {
    rv=$(rsync -a --rsh="ssh -i $opt_ssh_key_file " ./ hba-user@${archive_rshost}.office.hbaspecto.com:${full_archive_path}/ 2>&1 >/dev/null)
    if [[ $? == 0 ]]
    then
        echo ok
    else
        echo $rv
    fi
}

function compression_cmd {
    uname=$(uname)
    if [[ "${uname}" == "Darwin" ]]
    then
        if [[ -z $(which afsctool) ]]
        then
            echo "echo 'afsctool' not installed on this system. Please install from https://github.com/RJVB/afsctool \
                  to enable scenario compression."
        else
            echo "afsctool -c -j5 -6 $1"
        fi
    elif [[ "${systype}" == "windows" ]]
    then
        echo "cd $1 && compact //c //s && cd .."
    else
        echo "echo Scenario compression is currently only supported on OSX or Windows."
    fi
}

function showhelp {
    cat <<- EOM
        Usage:
                $0 [-h|--help] [-r|--restore] [SCENARIO]

        Options:
            -h,--help       Display this help
            -d,--delete-big Delete big files from local archive copy
            -n,--nocompress Don't compress local archive copy (compression only supported on OSX/Windows)
            -r,--restore    Restore backed up DB to centralized archive server specified in machine_settings.py
            SCENARIO        Scenario to backup/restore; if omitted, then read from pecas.yml

        Notes:
            * grep through script source for "big files" to see what files will be deleted
            * Compression only supported on OSX/Windows. Linux support coming - in the meantime, Linux files
               will always remain uncompressed. Script needs recursive write permissions on OSX to compress; may
               fail otherwise. Run script under sudo to avoid (or specify -n option).
EOM
}

##################################################################################################
# INITIAL ENVIRONMENT, ETC. SETUP
#################################

# SSH Setup:
#  - On the target machine and account, add the public key to ~/.ssh/authorized_keys
#  - The key (pecas-archive-scenario.key) needs to be unlocked (e.g.: to pecas-archive-scenario.unlocked)
#     on the source machine
#      + cp pecas-archive-scenario.key pecas-arvhive-scenario.unlocked
#      + ssh-keygen -p -f pecas-archive-scenario.unlocked

SCRIPT_DIR=$(builtin cd $(dirname ${BASH_SOURCE[0]}) ; pwd)
opt_ssh_key_file="$SCRIPT_DIR/pecas-archive-scenario/pecas-archive-scenario.unlocked"
set -o errexit

schemadir=${PWD##*/}

systype=$(getsystype)

schema=""
deletebig=false
compress=true
restore=false
while [ "$#" -gt 0 ]
do
    case "$1" in
        -r)           restore=true ; shift 1 ;;
        --restore)    restore=true ; shift 1 ;;

        -n)           compress=false ; shift 1 ;;
        --nocompress) compress=false ; shift 1 ;;

        -d)           deletebig=true ; shift 1 ;;
        --delete)     deletebig=true ; shift 1 ;;

        -h)           showhelp ; exit 1 ;;
        --help)       showhelp ; exit 1 ;;

        *)            schema="$1" ; shift 1 ;;
    esac
done

if [[ "$(uname)" == "Linux" ]]
then
    # Compression not supported on Linux. Yet.
    compress=false
fi

if [[ "${schema}" == "" ]]
then
    if [[ -f pecas.yml ]]
    then
        schema=$(stripspace $(grep sd_schema pecas.yml | cut -d: -f2))
    else
        echo "${0}: schema required! exiting"
        exit 1
    fi
fi

if [[ -f machine_settings.py ]]
then
    local_pghost=$(get_machinesetting str 'sd_host')
    local_pgport=$(get_machinesetting     'sd_port')
    local_pguser=$(get_machinesetting str 'sd_user')
    local_pgpass=$(get_machinesetting str 'sd_password')
    local_pgdb=$(get_machinesetting str 'sd_database')

    pgpath=$(get_machinesetting str 'pgpath')

    archive_rshost=$(get_machinesetting str 'archive_rshost')
    archive_rspath=$(get_machinesetting str 'archive_rspath')

    if $restore
    then
        archive_pghost=$(get_machinesetting str 'archive_dbhost')
        archive_pgport=$(get_machinesetting     'archive_dbport')
        archive_pguser=$(get_machinesetting str 'archive_dbuser')
        archive_pgpass=$(get_machinesetting str 'archive_dbpass')
        archive_pgdb=$(get_machinesetting str 'archive_dbname')
    fi
else
    echo "${0}: No machine_settings.py found! exiting"
    exit 1
fi

if [[ -z $(which rsync) ]]
then
    echo "${0}: rsync not found! exiting"
    if [[ "${systype}" == "windows" ]]
    then
        echo
        echo "(You can get a Windows/msys2 version of rsync from http://repo.msys2.org/msys/x86_64/)"
    fi
fi

if [[ "${systype}" == "windows" ]]
then
    dumpcmd=${pgpath}"pg_dump.exe"
    psqlcmd=${pgpath}"psql.exe"
    restorecmd=${pgpath}"pg_restore.exe"
else
    dumpcmd=${pgpath}pg_dump
    psqlcmd=${pgpath}psql
    restorecmd=${pgpath}pg_restore
fi

if [[ ! -d sd_backup ]]
then
    if [[ -w . ]]
    then
        mkdir sd_backup
    else
        echo "${0}: current directory not writable! exiting"
        exit 1
    fi
else
    if [[ ! -w sd_backup ]]
    then
        echo "${0}: backup directory not writable! exiting"
        exit 1
    fi
fi

full_archive_path=/zfs/svc/mrsgui/pecas/${archive_rspath}/${schemadir}

##################################################################################################
# RUN THE ACTUAL ARCHIVING PROCESS
##################################

set_db ${local_pghost} ${local_pgport} ${local_pguser} ${local_pgpass} ${local_pgdb}

# Subversion sometimes leaves extra .pristine files around, get rid of them
if [[ -e .svn ]]
then
    svn cleanup .
fi

# rsync to backup server
ssh -i $opt_ssh_key_file hba-user@${archive_rshost}.office.hbaspecto.com mkdir -p ${full_archive_path}/
rsync_msg=$(rsync_cmd)
if [[ "$rsync_msg" == "ok" ]]
then
    rsync_ok=true
else
    rsync_ok=false
    echo $rsync_msg
fi

# delete big files from the latest run results
if $deletebig
then
    if $rsync_ok
    then
        find . -type f \( \
                -name FloorspaceZoneTotalMakeUse.bin \
            -or -name TechnologyChoice.csv \
            -or -name ZonalMakeUse.csv \
            -or -name flow_matrices.omx \
            -or -name TAZDetailedUse.csv \
            -or -name TAZDetailedMake.csv \
            -or -name "buying_*.zipMatrix" \
            -or -name "selling_*.zipMatrix" \
            -or -name "aa-event.log" \
        \) -delete
    else
        echo "${0}: rsync failed - NOT deleting big files."
    fi
fi

# Make the db archive
$dumpcmd -v -n ${schema} -F c -f sd_backup/${schema}_asrun.backup

echo "${0}: Dumped PECAS SD database to backup file"

# (Optionally) restore db archive to leduc (or elsewhere?)
if $restore
then
    set_db ${archive_pghost} ${archive_pgport} ${archive_pguser} ${archive_pgpass} ${archive_pgdb}
    $restorecmd -h ${archive_pghost} -v -p ${archive_pgport} -U ${archive_pguser} -d ${archive_pgdb} sd_backup/${schema}_asrun.backup
    set_db ${local_pghost} ${local_pgport} ${local_pguser} ${local_pgpass} ${local_pgdb}
    echo "${0}: Restored PECAS SD database to archive db"
else 
    echo "${0}: Not restoring PECAS SD database to archive db, relying on .backup file instead"
fi


# Mark local db copy as backed up
$psqlcmd -c "alter schema ${schema} rename to ${schema}_backedup;"

if ! $rsync_ok
then
    echo "${0}: rsync FAILED. Please run manually to ensure scenario is backed up properly."
    exit 1
fi

echo "${0}: Refreshing archive filesystem backup to include SD database .backup file"

# rsync again, to send the db over (obviously don't bother if the rsync failed last time)
rsync_msg=$(rsync_cmd)
if [[ "$rsync_msg" == "ok" ]]
then
    rsync_ok=true
else
    rsync_ok=false
    echo $rsync_msg
fi

# ...and done. Mark folder as backed up and exit.

# Note that the following cd is necessary to allow the script to mv the directory. But unless
#  the entire script is sourced (via, e.g., the command: "source pecas-archive-scenario.sh")
#  then once the script exits, one will be left in the parent shell process, which will still
#  be in the (now nonexistent) original directory name. Not a big deal, user just needs to
#  manually "cd .." to get out of it. Or source the script, as above. Or run the script from
#  the parent directory to begin with, etc.
cd ..

mv ${schemadir} ${schemadir}_backedup
if $compress
then
    echo "${0}: Compressing scenario directory"
    $(compression_cmd ${schemadir}_backedup)
else 
    echo "${0}: Not compressing scenario directory"
fi
