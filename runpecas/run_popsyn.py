# Script to run the population synthesizer manually for a given year.
# Pass the year as the command line argument.
import sys

import runpecas.run_aa as aa
import pecas_routines as pr


def main():
    year = int(sys.argv[1])

    ps = pr.load_pecas_settings()

    runner = aa.StandardAARunner()
    runner.popsyn.start()
    runner.run_popsyn(ps, year)
    runner.popsyn.join()
