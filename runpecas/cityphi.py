# Functions for working with CityPhi
import logging

import pecas_routines as pr

from psycopg2 import extras

curfact = extras.RealDictCursor


def crosstab(ps, lastyear=None):
    recreate_crosstab(ps)

    for year in ps.snapshotyears:
        logging.info("Adding parcels from year {} to the crosstab".format(year))
        if (lastyear is None or year <= lastyear) and year > ps.baseyear:
            add_year_to_crosstab(ps, year, use_snapshots=True)


def recreate_crosstab(ps):
    with pr.connect_to_sd(ps) as conn, conn.cursor() as cur:
        cur.execute(
            "drop table if exists {sch}.parcel_histories".format(
                sch=ps.sd_schema))

        query = ("create table {sch}.parcel_histories as\n"
                 "select pecas_parcel_num,\n"
                 "pb.space_type_id as space_type_{yr},\n"
                 "pb.space_quantity as space_quantity_{yr},\n"
                 "pb.land_area as land_area_{yr}\n"
                 "from {sch}.parcels_backup_with_geom pb").format(
            sch=ps.sd_schema, yr=ps.baseyear)

        cur.execute(query)

        cur.execute("alter table {sch}.parcel_histories "
                    "add primary key (pecas_parcel_num)".format(
            sch=ps.sd_schema))
    conn.close()


def add_year_to_crosstab(ps, year, use_snapshots=False):
    tbl = "parcels_{yr}" if use_snapshots else "parcels"

    with pr.connect_to_sd(ps) as conn, conn.cursor() as cur:
        query = ("alter table {sch}.parcel_histories\n"
                 "add column space_type_{yr} integer,\n"
                 "add column space_quantity_{yr} double precision,\n"
                 "add column land_area_{yr} double precision").format(
            sch=ps.sd_schema, yr=year)

        cur.execute(query)

        query = ("update {sch}.parcel_histories ct\n"
                 "set space_type_{yr} = pyr.space_type_id,\n"
                 "space_quantity_{yr} = pyr.space_quantity,\n"
                 "land_area_{yr} = pyr.land_area\n"
                 "from {sch}." + tbl + " pyr\n"
                                       "where ct.pecas_parcel_num = pyr.pecas_parcel_num").format(
            sch=ps.sd_schema, yr=year)
        cur.execute(query)

    conn.close()


def advance_crosstab(ps, lastyear=None):
    with pr.connect_to_sd(ps) as conn, conn.cursor(cursor_factory=curfact) as cur:
        query = "select * from {sch}.parcel_histories limit 1".format(sch=ps.sd_schema)
        cur.execute(query)
        result = cur.fetchone()

    conn.close()

    for year in ps.snapshotyears:

        if (lastyear is None or year <= lastyear) and "space_type_{yr}".format(yr=year) not in result:
            add_year_to_crosstab(ps, year)


if __name__ == "__main__":
    main_ps = pr.load_pecas_settings()
    pr.set_up_logging(main_ps)
    crosstab(main_ps, None)
