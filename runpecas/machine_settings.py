from hbautil import pecassetup


def main():
    try:
        fname = pecassetup.local_machine_settings_fname()
        with open(fname, "x") as f:
            f.write(
                "# Machine settings for {machine}; these override the default\n"
                "# settings in machine.yml when running on {machine}.".format(
                    machine=pecassetup.machine_name()
                )
            )
        print("Created {} for settings specific to this machine".format(fname))
    except FileExistsError:
        pass


if __name__ == "__main__":
    main()
