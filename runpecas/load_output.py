import argparse

import hbautil.mapit_files as mf
import hbautil.scriptutil as su
import pecas_routines as pr
from hbautil import mapit
from hbautil import settings
from hbautil.pecassetup import set_up_logging


def sequence_str(sequence):
    return "{} ({})".format(sequence.num, sequence.name)


def main():
    parser = argparse.ArgumentParser(
        description=
            "Uploads PECAS output files to Mapit. "
            "Valid filenames are: " + ", ".join(mf.mapit_files_by_name) + ". "
            "Valid sequences are: " + ", ".join(mf.mapit_sequences_by_name) + "."
    )

    parser.add_argument(
        "files",
        help="files or sequences to upload, separated by commas")
    parser.add_argument(
        "years", type=su.RangeList,
        help="years to upload, as comma-separated ranges; "
             "for example, 2011-2016,2020,2030-2032 will upload 2011 through 2016, 2020, and 2030 through 2032")
    parser.add_argument(
        "-i", "--iface",
        help="specify interface, either 'sql' or 'http', if not specified uses settings from pecas.yml")
    args = parser.parse_args()
    files = args.files.split(",")
    years = args.years.list
    iface = args.iface

    ps = pr.load_pecas_settings()
    set_up_logging(ps)

    if iface is None:
        iface = ps.load_output_to_mapit
        if iface == False: # in case the user is calling load_output manually from this main() method, we want to default to true
            iface = True
    sequences = {}
    custom_sequence = []
    for file in files:
        try:
            sequence = mf.mapit_sequences_by_name[file]
            sequences[sequence] = mf.mapit_upload_sequences[sequence]
        except KeyError:
            custom_sequence.append(mf.mapit_files_by_name[file])
    if custom_sequence:
        sequences["Custom"] = custom_sequence

    load_settings = settings.from_module(mf).clone_with(
        load_output_to_mapit=iface,
        mapit_upload_sequences=sequences
    )
    ps = ps.clone_with(**load_settings.as_dict)

    mapit.clear_specific_uploads(ps, years, sequences)
    for year in years:
        print("Uploading {} for year {}".format(", ".join(files), year))
        for sequence in sequences:
            print("Loading sequence "+str(sequence))
            mapit.load_outputs_for_year(ps, year, sequence)


if __name__ == "__main__":
    main()
