import csv
import logging
from os.path import join

import hbautil.csvutil as cu
import pecas_routines as pr
import hbautil.mapit_files as mf
from hbautil.sqlutil import Querier


def update_activity_totals(ps, year):
    logging.info("Running PECAS ED")
    logging.info("Scaling ActivityTotals based on consumer surplus")
    querier = pr.mapit_querier(ps)
    consumer_surplus = querier.query(
        "select activity, consumersurpluschange\n"
        "from output.consumer_surplus\n"
        "where scen1 = %(base)s and scen2 = %(scen)s\n"
        "and year_run = %(year)s",
        scen=ps.scenario, base=ps.scenario_ed_base, year=year
    )
    surp_dict = {k: v for k, v in consumer_surplus}

    activity_types = querier.query(
        "select an.activity, atyp.activity_type_name\n"
        "from output.activity_numbers an\n"
        "join output.activity_types atyp\n"
        "using (activity_type_id)"
    )
    act_type_dict = {k: v for k, v in activity_types}

    acttot = read_acttot(ps, year)
    coeffs = read_coeffs(ps)

    for row in acttot:
        actname, quantity = row[:2]
        acttype = act_type_dict[actname]
        row[1] += surp_dict[actname] * coeffs.get(acttype, 0.0)

    write_acttot(ps, acttot, year)

    if ps.load_output_to_mapit:
        pr.load_outputs_for_year(ps, year, mf.EAGER_ED)


def read_acttot(ps, year):
    fname = join(ps.scendir, str(year), "ActivityTotalsI.csv")
    with open(fname, 'r') as inf:
        reader = csv.reader(inf)
        next(reader)  # Skip header
        return [[k, float(v)] for k, v in reader]


def read_coeffs(ps):
    fname = join(ps.scendir, ps.inputpath, "GrowthPerBenefit.csv")
    return cu.read_dict(fname, "ActivityTypeName", "Coefficient", value_f=float)


def write_acttot(ps, acttot, year):
    fname = join(ps.scendir, str(year), "ActivityTotalsI_ED.csv")
    with open(fname, 'w', newline='') as outf:
        writer = csv.writer(outf)
        writer.writerow(["Activity", "TotalAmount"])
        for row in acttot:
            writer.writerow(row)

    fname = activity_totals_ed_output_path(ps)
    with open(fname, 'a', newline='') as outf:
        writer = csv.writer(outf)
        for row in acttot:
            writer.writerow([year] + row)


def begin_all_acttot(ps):
    fname = activity_totals_ed_output_path(ps)
    with open(fname, 'w', newline='') as outf:
        writer = csv.writer(outf)
        writer.writerow(["year_run", "activity", "total_amount"])


def dump_updated_activity_totals(ps, connect):
    querier = Querier(connect)

    querier.dump_to_csv(
        "select year_run, activity, total_amount\n"
        "from output.all_activity_totals_ed\n"
        "where scenario = %(scen)s\n"
        "order by year_run, activity",
        activity_totals_ed_input_path(ps),
        scen=ps.scenario_ed_inputs
    )


def activity_totals_ed_output_path(ps):
    return join(ps.scendir, ps.outputpath, "All_ActivityTotalsI_ED.csv")


def activity_totals_ed_input_path(ps):
    return join(ps.scendir, ps.inputpath, "All_ActivityTotalsI_ED.csv")
