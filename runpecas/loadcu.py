from os.path import join

import hbautil.scriptutil as su
import pecas_routines as pr


def main():
    ps = pr.load_pecas_settings()
    querier = pr.sd_querier(ps)
    
    with querier.transaction() as tr:
        sptypes = [row[0] for row in tr.query("select space_type_id from {sch}.space_types_i")]

        tr.query("drop table if exists {sch}.choice_utilities")
        tr.query("drop table if exists {sch}.choice_utilites_history")

        new_space_utility_cols = ",\n   ".join(
            ["new_space_{}_utility double precision".format(sptype) for sptype in sptypes])
        new_space_prob_cols = ",\n   ".join(
            ["new_space_{}_prob double precision".format(sptype) for sptype in sptypes])

        choice_utility_cols = (
            "   actual_event_type character(2),\n"
            "   no_change_utility double precision,\n"
            "   demolish_utility double precision,\n"
            "   derelict_utility double precision,\n"
            "   renovate_utility double precision,\n"
            "   add_space_utility double precision,\n"
            "   " + new_space_utility_cols + ",\n"
            "   change_computil double precision,\n"
            "   decay_computil double precision,\n"
            "   growth_computil double precision,\n"
            "   build_computil double precision,\n"
            "   new_space_computil double precision,\n"
            "   no_change_prob double precision,\n"
            "   demolish_prob double precision,\n"
            "   derelict_prob double precision,\n"
            "   renovate_prob double precision,\n"
            "   add_space_prob double precision,\n"
            "   " + new_space_prob_cols + "\n"
        )

        tr.query(
            "create table {sch}.choice_utilities\n"
            "(\n"
            "   old_pecas_parcel_num bigint,\n"
            "   new_pecas_parcel_num bigint primary key,\n"
            + choice_utility_cols +
            ")"
        )

        tr.query(
            "create table {sch}.choice_utilities_history\n"
            "("
            "   year_run integer\n"
            ")"
        )

        for year in range(ps.sd_startyear, ps.stopyear):
            print("Loading choice utilities for year {}".format(year))
            fname = join(str(year), "choiceUtilities.csv")
            
            fixed_fname = fix_neginf(fname)
            
            tr.query("truncate table {sch}.choice_utilities")
            try:
                tr.load_from_csv("{sch}.choice_utilities", fixed_fname)
            except IOError:
                print("No choice utilities for year {}".format(year))
                pass


def fix_neginf(fname):
    fixed_fname = su.backup_name(fname, "_neginf")
    
    with open(fname, "r") as inf:
        with open(fixed_fname, "w") as outf:
            outf.write(inf.read().replace("neginf", "-infinity"))
    
    return fixed_fname


if __name__ == "__main__":
    main()
