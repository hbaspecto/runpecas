import argparse
import csv
import logging
import shutil
import subprocess
import threading
import bisect
from os import path
from queue import Queue

import hbautil.scriptutil as su
import pecas_routines as pr
import pecas_routines.employment as emp

from pecas_routines.pecas_apply_optionchanges import (
    OptionChanges,
    )

import hbautil.mapit_files as mf

from hbautil.classpath import build_classpath_in_path, build_classpath, vm_properties

# These files are copied from the previous AA run in years where AA didn't run
output_file_list = [
    "AAExchangeResults.csv",
    "ExchangeResults.csv",
    "TechnologyChoice.csv",
    "CommodityNumbers.csv",
    "ZonalMakeUse.csv",
    "ActivityLocations.csv",
    "ActivityLocations2.csv",
    "TAZDetailedUse.csv",
    "TAZDetailedMake.csv",
    "CommodityZUtilities.csv",
    "ExchangeResultsTotals.csv",
    "ActivityNumbers.csv",
    "Histograms.csv",
    "ActivitySummary.csv",
    "MakeUse.csv",
    "FloorspaceDelta.csv",
    "XVector.csv",
]


# noinspection PyMethodMayBeStatic
class StandardAARunner(pr.AARunner):
    def __init__(self):
        self.popsyn = StandardAARunner.PopSynRunner()

    def __del__(self):
        self.popsyn.stop()

    def run_aa(self, ps, year, skimfile, skimyear, dbyear=None, load=True):
        if dbyear is None:
            dbyear = year

        if ps.scenario_base is not None:
            import_exchange_results(ps, ps.scenario_base, year)
            import_constants(ps, ps.scenario_base, year)

        # Optional floorspace adjustments and import/export calculations
        if ps.floorspace_calc_delta:
            pr.write_floorspace_i(ps, year)

        if ps.calculate_import_export_size:
            if year != ps.baseyear:
                pr.update_exporters(ps, dbyear)
                pr.update_importers(ps, dbyear)
                pr.write_activity_totals(ps, year, dbyear)

        owc_path = path.join(str(year), "OptionChanges.csv")
        if path.exists(owc_path):
            owc = OptionChanges(owc_path)
            input_path = path.join(str(year), "TechnologyOptionsSpecified.csv")
            if not path.exists(input_path):
                input_path = path.join(str(year), "TechnologyOptionsScaled.csv")
            if not path.exists(input_path):
                input_path = path.join("AllYears", "Inputs", "TechnologyOptionsI.csv")
            if path.exists(input_path):
                logging.info(f"Applying OptionChanges.csv to file {input_path}")
                owc.apply_option_weights(
                    input_path=input_path,
                    output_path=path.join(str(year),"TechnologyOptionsI.csv")
                )
            else:
                logging.fatal(f"OptionChanges.csv specified in {year} but no file to change")
                raise Exception(f"OptionChanges.csv specified in {year} but no file to change")
        else:
            logging.warning(f"Not doing OptionWeightChanges for {year}")
        args, kwargs = self.make_aa_program_call(ps, year, skimyear, skimfile)

        retcode = subprocess.call(*args, **kwargs)
        pr.copy_program_log_file_into_year(ps, "aa", year)
        # If this is a zero-iteration run, we're not expecting AA to converge, so don't crash when it doesn't!
        allowed_retcodes = [0, 2] if ps.aa_max_iterations == 0 else [0]
        pr.log_results_from_external_program(
            "AA model finished for year {}".format(year),
            "AA model did not run successfully in year {}".format(year),
            (retcode,),
            allowed_retcodes=allowed_retcodes
        )

        pr.copy_prices(ps, year)

        if ps.employment:
            self.popsyn.register_tm_input("Emp")
            self.popsyn.push(StandardAARunner._Task(ps, "Emp", self.run_employment, year))

        if year in ps.travel_model_input_years:
            self.popsyn.register_tm_input("Popsyn")
            self.popsyn.push(StandardAARunner._Task(ps, "Popsyn", self.run_popsyn, year))

        # Load critical AA output to output database for mapit and for generating travel model totals
        if ps.load_output_to_mapit and load:
            pr.load_outputs_for_year(ps, year, mf.EAGER_AA)

            # Write Travel Model Inputs
            #
            # There are two standard ways for PECAS to send results to a travel model.  The
            # first is to have PECAS AA develop a trip matrix based on commodity flows.  This is controlled by the
            # aa.properties file aa.makeTripMatrices=true (see
            # #http://projects.hbaspecto.com/groups/buildingapecasmodel/wiki/28bc1
            # /Demonstration_model_trip_generation_and_distribution.html
            #
            # The second standard way is to allocate total amounts of categories for the travel model using
            # the ActivityLocations2.csv output file.  This is done here in this script if
            # allocate_tm_totals is set to true in pecas.yml.
            if ps.allocate_tm_totals:
                if year != ps.baseyear:
                    self.allocate_tm(ps, year)

        if ps.allocate_tm_totals and (not ps.load_output_to_mapit or not load):
            logging.warning("Can't calculate travel model totals, as results not loaded to mapit")

    def make_aa_program_call(self, ps, year, skimyear, skimfile):
        """
        Returns the arguments that should be passed to subprocess.call to run the AA module, as a pair
        containing the positional arguments as a list and the keyword arguments as a dictionary.
        """

        classpath = build_classpath_in_path(
            path.join(ps.scendir, ps.codepath),
            specific_jars=[
                ps.pecasjar, ps.commonbasejar, ps.simpleormjar
            ],
            other_jars=[
                "or", "mtj", "log4j-1.2-api",
                "log4j-api", "log4j-core", "postgresql",
                "omx", "commons-math",
                "netlib-java", "arpack-combo", 'arpack_combined_all',
            ],
        )

        logging.info("classpath is " + classpath)

        logging.info("Starting PECAS AA Model run for year " + str(year))
        # Run AA
        vm_args = [
            "-Xmx" + ps.aa_memory,
            "-cp", build_classpath(path.join(ps.scendir, "AllYears", "Inputs"), classpath)
        ]

        self.add_properties(ps, vm_args, year, skimyear, skimfile)

        if ps.profiling:
            vm_args.append("-Xrunhprof:cpu=samples,file=hprof.txt,depth=24,thread=y")
        if False and ps.missionControl:
            vm_args.append("-XX:+UnlockCommercialFeatures")
            vm_args.append("-XX:+FlightRecorder")
            vm_args.append("-XX:+UnlockDiagnosticVMOptions")
            vm_args.append("-XX:+DebugNonSafepoints")

        logging.info("Running AA with " + " ".join(vm_args))

        return [
            [ps.javaRunCommand] +
            vm_args +
            ["com.hbaspecto.pecas.aa.control.AAControl"]
        ], {}

    def add_properties(self, ps, vm_args, year, skimyear, skimfile):
        if year in ps.constrainedyears:
            constrained = True
        else:
            constrained = False

        prevyear = year - 1
        logging.debug("prevyear =" + str(prevyear))

        properties = dict(
            {
                "log4j.configuration": "log4j.xml",
                "java.library.path": pr.get_native_path(),
            },
            SCENDIR=ps.scendir,
            YEAR=year,
            SKIMYEAR=skimyear,
            SKIMFILE=skimfile,
            PREVYEAR=prevyear,
            YEARSINCELASTRUN=_year_since_last_run(ps, year),
            CONSTRAINED=constrained,
            GROUPBOUNDS=ps.aa_group_bounds,
            MAXITS=ps.aa_max_iterations,
            INITSTEP=ps.aa_initial_step_size,
            MINSTEP=ps.aa_minimum_step_size,
            MAXSTEP=ps.aa_maximum_step_size,
            FSZONES=ps.use_floorspace_zones,
            MAXTOTAL=ps.max_total_clearance,
            MAXSPEC=ps.max_specific_clearance,
            AZVI=ps.use_activities_zonal_values_i,
            CONSTSIZE=ps.update_construction_size_terms,
            XVECTORCONSTANTS=year > ps.baseyear and ps.xvector_constants,
            TRIPMAT=(year in ps.travel_model_input_years) and ps.make_trip_matrices,
            POISSON=ps.poisson_trips,
            EMMETRIPS=ps.emme_format_trips,
            SECONDRUN=(not (constrained and ps.aa_max_iterations == 0)),
            STOPMISMATCH=ps.stop_on_constraint_mismatch,
            MAXTHREADS=getattr(ps, 'aa_maxthreads', 12),
            PUTTAX=ps.put_taxes,
            FLOWMATRICES=ps.writeFlowMatrices,
        )

        vm_args.extend(vm_properties(properties))

    def allocate_tm(self, ps, year):
        import psycopg2 as pg
        with pr.connect_to_mapit(ps) as conn, conn.cursor() as cur:
            query = "select input.generate_tm_inputs(%s,%s);"
            params = [year, ps.scenario]
            logging.info("Executing query: " + str(cur.mogrify(query, params)))
            try:
                cur.execute(query, params)
            except pg.Error as e:
                logging.error(
                    "Problem preparing table of travel model "
                    "inputs in database: " + str(e))
                raise

            out_path = path.join(ps.scendir, str(year), "tm_inputs.csv")
            with open(out_path, "w") as out_file:
                try:
                    cur.copy_expert(
                        "copy input.tm_input to stdout csv header", out_file)
                except pg.Error as e:
                    logging.error(
                        "Problem writing travel model land-use inputs for " +
                        str(year) + ": " + str(e))
                    raise

    def run_employment(self, ps, year):
        logging.info("Calculating employment for year {}".format(year))

        def connect():
            return pr.connect_to_sd(ps)

        # noinspection PyBroadException
        try:
            emp.main(ps, connect, year)
            if ps.load_output_to_mapit:
                pr.load_outputs_for_year(ps, year, mf.LAZY_EMP)
        except Exception as e:
            logging.error("Employment calculations failed: {}".format(e))

    def run_popsyn(self, ps, year):
        logging.info("Running the population synthesizer for year {}".format(year))
        # noinspection PyBroadException
        try:
            _run_popsyn(ps, year)
        except Exception as e:
            logging.error("Population synthesizer failed: {}".format(e))

    class PopSynRunner(threading.Thread):

        def __init__(self):
            super().__init__()
            self.q = Queue()
            self._stop_ = threading.Event()
            self._tm_inputs = set()
            self._tm_input_events = None
            self._tm_inputs_created = None

        def init_tm_input_years(self, years):
            if self._tm_input_events is None:
                self._tm_input_events = {year: threading.Event() for year in years}
                self._tm_inputs_created = {year: set() for year in years}

        def push(self, task):
            self.q.put(task)

        def stop(self):
            self._stop_.set()

        def run(self):
            # noinspection PyBroadException
            try:
                while True:
                    if self._stop_.is_set():
                        break

                    if not self.q.empty():
                        task = self.q.get()
                        if task is None:
                            break
                        else:
                            task.run()
                            if self._tm_input_events is not None and task.year in self._tm_input_events:
                                self._tm_inputs_created[task.year].add(task.name)
                                if not self._tm_inputs - self._tm_inputs_created[task.year]:
                                    logging.info("{} created".format(task.name))
                                    logging.info("Still waiting for {}".format(
                                        self._tm_inputs - self._tm_inputs_created[task.year]))
                                    self._tm_input_events[task.year].set()
            except Exception as e:
                logging.error("Population synthesis thread failed: {}".format(e))

        def register_tm_input(self, name):
            self._tm_inputs.add(name)

        def wait_for_tm_inputs(self, year):
            if self._tm_inputs - self._tm_inputs_created[year]:
                logging.info("Waiting for {}".format(self._tm_inputs - self._tm_inputs_created[year]))
                self._tm_input_events[year].wait()

    class _Task:
        def __init__(self, ps, name, method, year):
            self.name = name
            self.method = method
            self.year = year
            self.ps = ps

        def run(self):
            self.method(self.ps, self.year)


def _year_since_last_run(ps, year):
    i = bisect.bisect_left(ps.aayears, year) - 1
    if i < 0:
        i = 0
    return year - ps.aayears[i]


def _run_popsyn(ps, year):
    scendir = path.abspath(ps.scendir)
    targets_fname = "PopSynTargets.csv"
    targets_fpath = path.join(scendir, str(year), targets_fname)
    _scale_popsyn_targets(ps, targets_fpath)
    popsyndir = path.abspath(path.join(ps.scendir, ps.popsyndir))
    shutil.copy(targets_fpath, path.join(popsyndir, targets_fname))

    prev_fname = "Output"+ps.popsyn_samples_file

    seed_fname = "Seed"+ps.popsyn_samples_file
    seed_path = path.join(scendir, str(year), seed_fname)
    if not path.exists(seed_path):
        seed_path = path.join(scendir, "AllYears", "Inputs", seed_fname)
        if not path.exists(seed_path):
            seed_path = None
    if seed_path:
        shutil.copy(seed_path, path.join(popsyndir, prev_fname))

    classpath = build_classpath_in_path(
        path.abspath(path.join(ps.scendir, ps.codepath)),
        specific_jars=[ps.popsynjar],
        other_jars=["log4j", "commons-math"]
    )
    vm_args = [
        "-Xmx" + ps.pop_syn_memory,
        "-cp", build_classpath(
            path.join(scendir, "AllYears/Inputs"),
            popsyndir,
            classpath
        )
    ]

    logging.info("Running pop synth for {} with {}".format(year, " ".join(vm_args)))
    retcode = subprocess.call(
        [ps.javaRunCommand] +
        vm_args +
        ["com.hbaspecto.synthesizer.GeneratePopulation", "Synth.properties"],
        cwd=popsyndir
    )
    pr.log_results_from_external_program(
        "Pop synth finished for year {}".format(year),
        "Pop synth did not run successfully in year {}".format(year),
        (retcode,)
    )

    shutil.copy(path.join(popsyndir, prev_fname), path.join(scendir, str(year), prev_fname))


def _scale_popsyn_targets(ps, fpath):
    try:
        scale_path = path.join(ps.scendir, "AllYears", "Inputs", "PopSynEmploymentScaleFactors.csv")

        with open(scale_path, 'r') as scale_f:
            reader = csv.reader(scale_f)
            next(reader)
            scale_factors = {k: float(v) for k, v in reader}

        su.backup(fpath, "_unscaled")

        with open(fpath, 'r') as target_f:
            reader = csv.reader(target_f)
            target_header = next(reader)
            target_rows = list(reader)

        taz_col = target_header.index("TAZ")
        emp_cols = [(i, col.split("_")[2]) for (i, col) in enumerate(target_header) if col.startswith("empin_occ")]

        for row in target_rows:
            if int(row[taz_col]) == 0:
                continue
            test_emp = row[emp_cols[0][0]]
            if test_emp == "" or float(test_emp) < 0:
                continue

            for col, code in emp_cols:
                scale_code = "NC" + code
                scale_factor = scale_factors[scale_code]
                row[col] = str(float(row[col]) * scale_factor)

        with open(fpath, 'w', newline="") as target_f:
            writer = csv.writer(target_f)
            writer.writerow(target_header)
            for row in target_rows:
                writer.writerow(row)
    except IOError:
        logging.info("PopSynEmploymentScaleFactors file not found, skipping this step")


def import_exchange_results(ps, scenario, year):
    logging.info("Importing ExchangeResults.csv from scenario " + scenario)
    querier = pr.mapit_querier(ps)
    dest_fname = path.join(ps.scendir, str(year), "ExchangeResultsI.csv")
    querier.dump_to_csv(
        "select commodity as \"Commodity\", zonenumber as \"ZoneNumber\", "
        "demand as \"Demand\", internalbought as \"InternalBought\", exports as \"Exports\", "
        "supply as \"Supply\", internalsold as \"InternalSold\", imports as \"Imports\", "
        "surplus as \"Surplus\", price as \"Price\", "
        "buyingsizeterm as \"BuyingSizeTerm\", sellingsizeterm as \"SellingSizeTerm\", "
        "derivative as \"Derivative\"\n"
        "from output.all_exchange_results\n"
        "where scenario = %(scen)s\n"
        "and year_run = %(year)s",
        dest_fname,
        scen=scenario, year=year
    )


def import_constants(ps, scenario, year):
    logging.info("Importing ActivityLocations.csv from scenario " + scenario)
    querier = pr.mapit_querier(ps)
    dest_fname = path.join(ps.scendir, str(year), "ActivitiesZonalValuesI.csv")
    querier.dump_to_csv(
        "select activity as \"Activity\", zonenumber as \"ZoneNumber\", "
        "quantity as \"Quantity\", technologylogsum as \"TechnologyLogsum\", "
        "sizeutility as \"SizeUtility\", zoneconstant as \"ZoneConstant\", "
        "constrained as \"Constrained\", constraintvalue as \"ConstraintValue\", "
        "locationutility as \"LocationUtility\", fullembeddedsize as \"FullEmbeddedSize\", "
        "size as \"Size\", zoneconstantforexchangesize as \"ZoneConstantForExchangeSize\"\n"
        "from output.all_activity_locations\n"
        "where scenario = %(scen)s\n"
        "and year_run = %(year)s",
        dest_fname,
        scen=scenario, year=year
    )


def main(ps, year, rerun=False):
    skimyear = pr.get_skim_year(year, ps.skimyears)
    skimfilename = ps.skim_fname.format(year=skimyear)

    aa = StandardAARunner()
    aa.popsyn.start()

    have_org = False
    if rerun:
        logging.info(f"Rerunning AA in {year} with solved prices from ExchangeResults.csv")
        have_org = True
        try:
            shutil.move(
                path.join(ps.scendir, str(year), "ExchangeResultsI.csv"),
                path.join(ps.scendir, str(year), "ExchangeResultsI-orig.csv")
            )
        except FileNotFoundError:
            have_org = False
        shutil.copyfile(
            path.join(ps.scendir, str(year), "ExchangeResults.csv"),
            path.join(ps.scendir, str(year), "ExchangeResultsI.csv")
        )

    try:
        logging.info(f"Running AA in year {year}")
        # noinspection PyTypeChecker
        aa.run_aa(ps, year, skimfilename, skimyear, dbyear=None, load=False)

        if have_org:
            shutil.move(
                path.join(ps.scendir, str(year), "ExchangeResultsI-orig.csv"),
                path.join(ps.scendir, str(year), "ExchangeResultsI.csv")
            )
    finally:
        aa.popsyn.stop()


def get_args_and_run():
    parser = argparse.ArgumentParser(
        description="Runs the PECAS AA module for one year."
    )
    parser.add_argument("year", type=int, help="The year to run AA for.")
    parser.add_argument(
        "-s", "--settings", default="pecas.yml", help="The YAML file that PECAS should get its settings from")
    parser.add_argument("-r", "--rerun", action="store_true", help="Reruns the model with already solved prices")

    args = parser.parse_args()

    ps = pr.load_pecas_settings(args.settings)
    pr.set_up_logging(ps)
    _year = args.year
    if args.rerun:
        _rerun=True
    else:
        _rerun=False
    main(ps,_year,rerun=_rerun)


if __name__ == "__main__":
    get_args_and_run()
