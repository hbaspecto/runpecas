import argparse
import logging
import shutil
import sys
import threading
from os.path import join, exists
from queue import Queue
from typing import List

from runpecas import cityphi
from hbautil import propedit, scriptutil as su
import pecas_routines as pr
import pecas_routines.phasing as ph
#TODO we should rename reset_database to something else if we're only using it to clear the mapit database
import pecas_routines.reset_database as rd
import hbautil.sd_backup_restore as sbr
import pecas_routines.skims_to_sem as skims
import hbautil.mapit_files as mf
import runpecas.run_aa as aa
import runpecas.run_ed as ed
import runpecas.run_sd as sd
import techscaling.update_techopt as uto


class MapitLazyLoader(threading.Thread):
    instances = []

    q = None
    ps = None
    load = None
    name = "MapIt Lazy Loader"

    def __init__(self, ps, q, load=None):
        logging.info("Starting MapIt Lazy Loader consumer thread")

        super(MapitLazyLoader, self).__init__()
        self.q = q
        self.load = load or pr.load_outputs_for_year
        self.ps = ps

        self._stop_ = threading.Event()

        MapitLazyLoader.instances.append(self)

    def stop(self):
        logging.info("Halting MapIt Lazy Loader consumer thread")
        self._stop_.set()
        self.unregister()

    @property
    def stopped(self):
        return self._stop_.is_set()

    def unregister(self):
        try:
            i = MapitLazyLoader.instances.index(self)
            del MapitLazyLoader.instances[i]
        except ValueError:
            pass

    def __del__(self):
        self.unregister()

    def run(self):
        while True:
            if self.stopped:
                # Abort! Abort!
                break

            if not self.q.empty():
                job = self.q.get()
                if job is None:
                    # Flag to indicate PECAS is done generating data
                    break

                logging.info("Lazily loading sequence {0[sequence]} in {0[year]} to MapIt".format(job))
                self.load(self.ps, **job)


# noinspection PyBroadException,PyUnboundLocalVariable
def main(ps, aa_runner=aa.StandardAARunner(), sd_runner=sd.StandardSDRunner(), tm_runner=None):
    try:
        if ps.use_tm and tm_runner is None:
            import runpecas.run_tm as tm
            tm_runner = tm.TMRunner()
        if ps.use_tm:
            ps.travel_model_input_years = sorted(
                set(ps.travel_model_input_years) |
                set(year for year in ps.tmyears if year >= ps.tm_startyear)
            )
        pre_check(ps, aa_runner, sd_runner, tm_runner)
        main_impl(ps, aa_runner, sd_runner, tm_runner)
    except Exception as e:
        import traceback
        logging.fatal(repr(e))
        for line in traceback.format_tb(e.__traceback__):
            logging.fatal(line)
        raise


# noinspection PyUnusedLocal
def pre_check(ps, aa_runner, sd_runner, tm_runner):
    """
    Does "pre-flight" checks - checks for common setup problems
    that might cause a crash hours or days later but can be identified now.
    """

    check_all_year_directories_exist(ps)
    check_pop_syn_base_targets_exist_in_pop_syn_years(ps)
    check_travel_model_available(ps, tm_runner)
    check_aa_running_in_travel_model_years(ps)


def check_all_year_directories_exist(ps):
    for year in su.irange(ps.baseyear, ps.stopyear):
        if not exists(join(ps.scendir, str(year))):
            raise PrecheckFailure(f"There is no year directory for the run year {year}")


def check_pop_syn_base_targets_exist_in_pop_syn_years(ps):
    for year in ps.travel_model_input_years:
        if not exists(join(ps.scendir, str(year), "PopSynBaseTargets.csv")):
            raise PrecheckFailure(f"There is no PopSynBaseTargets file for year {year}")


def check_travel_model_available(ps, tm_runner):
    if ps.use_tm:
        precheck_error = tm_runner.tm_precheck(ps)
        if precheck_error:
            raise PrecheckFailure(precheck_error)


def check_aa_running_in_travel_model_years(ps):
    if ps.use_tm:
        for year in ps.tmyears:
            if year not in ps.aayears:
                raise PrecheckFailure(f"The travel model is set to run in {year}, but AA isn't")


class PrecheckFailure(Exception):
    def __init__(self, msg):
        super().__init__(msg)


# noinspection PyBroadException
def main_impl(ps, aa_runner, sd_runner, tm_runner):
    logging.info("**********************")
    logging.info(
        f"Starting PECAS Run in {ps.scendir}. MapIt scenario {ps.scenario}, SD schema {ps.sd_schema}"
    )

    mapit_queue = None
    mapit_lazy_loader = None
    if ps.load_output_to_mapit:
        mapit_queue = Queue()  # conveniently thread-safe
        mapit_lazy_loader = MapitLazyLoader(ps, q=mapit_queue)
        mapit_lazy_loader.start()
    if ps.travel_model_input_years or ps.employment:
        aa_runner.popsyn.start()

    if ps.resume_run:
        try:
            start_year, skip_sd = read_resume()
            if ps.load_output_to_mapit:
                pr.clear_upload(ps, start_year)
        except (IOError, ValueError):
            logging.warning(
                "No valid resume file; exiting; you can set resume_run to false if you want to start over")
            raise
    else:
        start_year = ps.baseyear
        skip_sd = False
        if ps.reset_initial_database:
            reset_database(ps, ps.aa_startyear)
        if ps.generate_cityphi:
            try:
                cityphi.recreate_crosstab(ps)
            except Exception:
                # ignore - If SD didn't run at all, there will be no outputs anyway.
                pass
        if ps.load_output_to_mapit:
            rd.clear_mapit_outputs(ps)

        if ps.adaptive_phasing and ps.sd_backupyear == ps.baseyear:
            ph.clear_manual_zoning_in_phasing_plans(ps)
            ph.set_plan_phase_dict(ps)
            ph.update_parcel_zoning_xref_according_to_adaptive_phasing(ps, start_year)

        if ps.scenario_ed_base is not None:
            ed.begin_all_acttot(ps)

    if ps.use_tm:
        aa_runner.popsyn.init_tm_input_years(ps.travel_model_input_years)

    try:
        main_loop(ps, aa_runner, sd_runner, tm_runner, start_year, skip_sd, mapit_queue)
    finally:
        if ps.load_output_to_mapit:
            mapit_queue.put(None)
            logging.info("Done running PECAS. Waiting for MapIt lazy loader to finish.")
            mapit_lazy_loader.join()
            logging.info("Lazy loader finished.")

        if ps.travel_model_input_years or ps.employment:
            aa_runner.popsyn.push(None)
            logging.info("Waiting for the employment/population synthesizer thread")
            aa_runner.popsyn.join()
            logging.info("Employment/population synthesizer thread finished")


def main_loop(ps, aa_runner, sd_runner, tm_runner, start_year, skip_sd, mapit_queue):
    year = start_year

    project_code = load_project_code(ps)
    project_code.before_run(ps)

    converted_skims = set()

    run_ed = (ps.scenario_ed_inputs is not None) or ps.ed_from_files

    while year <= ps.stopyear:
        write_resume(ps, year)

        skimyear = pr.get_skim_year(year, ps.skimyears)
        skimfilename = ps.skim_fname.format(year=skimyear)

        project_code.start_of_year(ps, year)

        # --------------------------------------------------------------------------------------------------------------
        # AA module
        # --------------------------------------------------------------------------------------------------------------

        if year >= ps.aa_startyear:
            if ps.use_tm and skimyear >= ps.earliest_squeeze_year and skimyear not in converted_skims:
                tm_runner.retrieve_skims(ps, skimyear)
                if ps.squeeze_skims:
                    skims.main(ps, skimyear)
                converted_skims.add(skimyear)
            elif ps.squeeze_skims and skimyear >= ps.earliest_squeeze_year and skimyear not in converted_skims:
                skims.main(ps, skimyear)
                converted_skims.add(skimyear)

            if year in ps.aayears:
                aa_runner.run_aa(ps, year, str(skimfilename), skimyear)
            else:
                if year == ps.baseyear:
                    logging.error("You need to run AA in the base year, check aayears!")
                    raise ValueError
                else:
                    # need to copy all of the stuff from last year
                    for file in aa.output_file_list:
                        try:
                            shutil.copyfile(
                                join(ps.scendir, str(year - 1), file),
                                join(ps.scendir, str(year), file)
                            )
                        except IOError:
                            logging.warning(
                                f"Couldn't copy {file} from year {year - 1} to year {year}"
                            )
                    if ps.load_output_to_mapit:
                        pr.load_outputs_for_year(ps, year, mf.EAGER_AA)

            if ps.scale_technology_options and not run_ed and not ps.resume_run and year == ps.aa_startyear:
                uto.create_all_acttot_techopt(ps, lambda: pr.connect_to_sd(ps), first_year=ps.aa_startyear + 1,
                                              output_file='TechnologyOptionsScaled.csv')

        elif ps.load_output_to_mapit:
            # Normally the runAA command loads its output to mapit, but if AA didn't run we need to do it here
            pr.load_outputs_for_year(ps, year, mf.EAGER_AA)

        if ps.load_output_to_mapit and year >= ps.mapit_startyear:
            mapit_queue.put({
                'year': year,
                'sequence': mf.LAZY_AA
            })

        if ps.use_aa_to_sd_price_ratio and year == ps.sd_backupyear:
            pr.calculate_aa_to_sd_price_correction(ps,
                min_ratio=ps.min_price_correction_ratio,
                max_ratio=ps.max_price_correction_ratio)

        # Atlanta Specific
        if ps.allocate_am_totals:
            pr.write_abm_land_use(ps, year)
        if ps.labour_make_use:
            pr.write_labor_make_use(ps, year)

        project_code.after_aa(ps, year)

        # --------------------------------------------------------------------------------------------------------------
        # ED module
        # --------------------------------------------------------------------------------------------------------------

        if ps.scenario_ed_base is not None and not run_ed and year > ps.baseyear and year >= (ps.mapit_startyear or 0):
            ed.update_activity_totals(ps, year)

        if run_ed and year == ps.baseyear:
            if ps.scenario_ed_inputs is not None:
                ed.dump_updated_activity_totals(ps, lambda: pr.connect_to_mapit(ps))

            uto.create_all_acttot_techopt(
                ps,
                lambda: pr.connect_to_sd(ps),
                acttot_src=ed.activity_totals_ed_input_path(ps),
                output_file='TechnologyOptionsScaled.csv'
            )

        project_code.after_ed(ps, year)

        # --------------------------------------------------------------------------------------------------------------
        # TM module
        # --------------------------------------------------------------------------------------------------------------

        do_tm = (
            ps.use_tm and
            year in ps.tmyears and
            year >= ps.tm_startyear
        )
        if do_tm:
            logging.info("Starting PECAS TM Model run for year {}".format(year))
            aa_runner.popsyn.wait_for_tm_inputs(year)
            tm_runner.run_tm(ps, year)
            logging.info("TM model finished for year {}".format(year))

        project_code.after_tm(ps, year)

        # --------------------------------------------------------------------------------------------------------------
        # SD module
        # --------------------------------------------------------------------------------------------------------------

        do_sd = (
            ps.use_sd and
            ps.sd_backupyear <= year < ps.stopyear  # SD is not run or replayed in the last year
        )

        if do_sd:
            # this is only if we are resuming a run after a computer problem and SD has already run
            if skip_sd:
                skip_sd = False
            elif year >= ps.sd_startyear:
                if ps.use_aa_to_sd_price_ratio:
                    pr.apply_aa_to_sd_price_correction(ps, year)
                if year in ps.find_expected_values_years:
                    find_expected_values(ps, year)
                sd_runner.run_sd(ps, year, str(skimfilename), skimyear)
            else:
                pr.replay_development_events_for_year(ps, year)

                if ps.load_output_to_mapit:
                    pr.load_outputs_for_year(ps, year, mf.EAGER_SD)

            write_resume(ps, year, skip_sd=True)

            if ps.load_output_to_mapit and year >= ps.mapit_startyear:
                mapit_queue.put({
                    'year': year,
                    'sequence': mf.LAZY_SD
                })

            # TODO have SD read the SDPrices.csv file directly, rather than renaming ExchangeResults during SD's run
            if ps.use_aa_to_sd_price_ratio and year >= ps.sd_startyear:
                # Put uncorrected ExchangeResults back for the next AA run
                shutil.copyfile(
                    join(ps.scendir, str(year), "AAExchangeResults.csv"),
                    join(ps.scendir, str(year), "ExchangeResults.csv")
                )

            if (year + 1) in ps.snapshotyears:
                pr.snapshot_parcels(ps, year + 1)
                if ps.generate_cityphi:
                    cityphi.add_year_to_crosstab(ps, year + 1)

            if ps.adaptive_phasing:
                ph.update_parcel_zoning_xref_according_to_adaptive_phasing(ps, year + 1)

            write_floorspace_summary = (
                    ps.use_sd and
                    ps.sd_startyear > year >= ps.aa_startyear - 1 and
                    year < ps.endyear
            )
            if write_floorspace_summary:
                # AA is going to run next year, but SD didn't run. Prepare
                # FloorspaceO and FloorspaceSD.
                pr.write_floorspace_summary_from_parcel_file(ps, str(year + 1))
                pr.copy_floorspace_summary(ps, year + 1)

            if ps.labour_make_use:
                pr.write_labor_make_use(ps, year)

        year = year + 1

    project_code.after_sd(ps, year)


def read_resume():
    with open("resume.txt", "r") as resume_file:
        resume_code = next(resume_file).strip()
        skip_sd = False
        if resume_code.endswith("nosd"):
            skip_sd = True
            resume_code = resume_code[:-4]
        year = int(resume_code)
        return year, skip_sd


def write_resume(ps, resume_year, skip_sd=False):
    try:
        with open(join(ps.scendir, "resume.txt"), "w") as resume_file:
            resume_file.write(str(resume_year))
            if skip_sd:
                resume_file.write("nosd")
    except IOError:
        pass


def reset_database(ps, aa_startyear=None):
    sbr.restore_sd(ps, combine=True, schema=ps.sd_schema, threaded=False)
    if aa_startyear is not None and aa_startyear <= ps.baseyear:
        # Write out the base year FloorspaceI to ensure that the new run is
        # consistent with the database.
        # But DON'T DO THIS if we aren't running AA in the base year!
        pr.write_floorspace_summary_from_parcel_file(ps, ps.baseyear)
        pr.copy_floorspace_summary(ps, ps.baseyear)


def load_project_code(ps):
    sys.path.append(ps.scendir)
    # noinspection PyUnresolvedReferences
    import project_code
    return project_code


def find_expected_values(ps, year):
    def ip(fname):
        return join(ps.scendir, ps.inputpath, fname)

    shutil.copyfile(ip("sd.properties"), ip("sd_backup.properties"))

    try:
        pr.move_replace(ip("sd_calib.properties"), ip("sd.properties"))
        shutil.copyfile(ip("sd.properties"), ip("sd_calib.properties"))

        props = propedit.load_props(ip("sd.properties"))
        props.set_prop("CapacityConstrained", False)
        props.set_prop("LimitSpaceByTAZ", False)
        props.set_prop("sdorm.parcels", ps.expected_value_parcels)
        props.set_prop("EstimationTargetFile", ps.expected_value_targets)
        props.set_prop("EstimationMaxIterations", 0)
        props.new_prop("EstimationExpectedValuesOnly", True)
        props.save_props(ip("sd.properties"))

        pr.move_replace(ip("sd.properties"), ip("sd_ev_edited.properties"))
        shutil.copyfile(ip("sd_ev_edited.properties"), ip("sd.properties"))

        import sdcalib
        sdcalib.main(ps, year)
        pr.move_replace(
            join(ps.scendir, "event.log"), join(ps.scendir, str(year), "ev_event.log"))
        pr.move_replace(
            join(ps.scendir, "expected_values.csv"), join(ps.scendir, str(year), "expected_values.csv"))
    finally:
        pr.move_replace(ip("sd_backup.properties"), ip("sd.properties"))


def _pull(seq: List[str], item: str) -> bool:
    if item in seq:
        seq.remove(item)
        return True
    else:
        return False


def parse_arguments_and_run():
    parser = argparse.ArgumentParser(
        description="Runs the PECAS model."
    )
    parser.add_argument(
        "-s", "--settings", default="pecas.yml", help="The YAML file that PECAS should get its settings from"
    )
    parser.add_argument("-r", "--resume", action="store_true", help="Resumes from where the model last stopped")
    parser.add_argument(
        "-R", "--norestore", action="store_true", help="Turns off restoring the database from the filesystem"
    )
    parser.add_argument("-T", "--notech", action="store_true", help="Turns off tech scaling")
    parser.add_argument(
        "-t", "--notemp", action="store_true", help="Turns off creating the temporary parcel table in SD"
    )

    args = parser.parse_args()

    ps = pr.load_pecas_settings(args.settings)
    pr.set_up_logging(ps)

    setting_overrides = {}
    if args.resume:
        setting_overrides["resume_run"] = True
    if args.norestore:
        setting_overrides["reset_initial_database"] = False
    if args.notech:
        setting_overrides["scale_technology_options"] = False
    if args.notemp:
        setting_overrides["create_temp_parcels"] = False

    ps = ps.clone_with(**setting_overrides)

    logging.info("Running with settings file {!r}".format(args.settings))
    main(ps)
    logging.info("Done!")


if __name__ == "__main__":
    parse_arguments_and_run()
