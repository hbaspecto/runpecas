# This is the configuration file the Skim Squeezing
# feature in PECAS, which converts TAZ-level skims
# from the travel model to LUZ-level skims
# that PECAS can understand.

# Usually this is project-specific
# but not scenario-specific: it's defined once
# at the beginning of the project, and may be adjusted
# slightly as the project progresses, but otherwise
# it should be committed to the scenario repository
# and allowed to be automatically copied from scenario
# to scenario by the SVN copy operation.

# There are examples for Alberta and San Diego
# in test/skims_to_sem


# squeezes is the top-level key, which holds everything
# else in the file.

# Under squeezes should be a list, each giving the
# rules for converting one set of skim files from
# TDM to PECAS format. If multiple squeezes are
# specified, each squeeze will be done independently,
# then all of them will be combined into a single
# output file.

# Each squeeze has the following keys:

# - skim_names: The list of names that will be used
#   internally for the skims, i.e. what the columns
#   will be called in the PECAS database.
#
# - taz_skim_renames (optional): A mapping from
#   skim names found in the input file to internal
#   names. Only include the skim names that are
#   different from the input file names.
#
# - logsum_skims (optional): A list of skims
#   that represent logsums (where greater numbers are better)
#   rather than costs (where smaller numbers are better).
#
# - external_skim_names: The list of skims that are available
#   between internal and external zones (in WorldZoneSkims.csv)
#   Note the columns must be in the order specified here.
#
# - skims_to_external_skims: A mapping
#   from skim names to the corresponding external skims.
#   This should be a complete list of skims, as there
#   seems to be a bug in mapping the world market skims
#   if this is not complete. 
#
# - best_external_skim_names: The external skims that should
#   be used to choose which entry point to use.
#
# - transit_access: How access to transit stations should
#   be incorporated. Use this if the input skims are
#   for station-to-station transit trips. See the San Diego
#   example for the format.
#
# - taz_skims_fname (optional): The name of the input skim
#   file. Defaults to what's in pecas.yml.
#
# - luz_skims_fname (optional): The name of the output skim
#   file. Defaults to what's in pecas.yml, as given by
#   the skim_fname setting.
#
# - omx_fname (optional): If provided, skims_to_sem will
#   extract the skims from the specified OMX file, and
#   dump them to taz_skims_fname before reading them in
#   to do the squeezing.
#
# - table_suffix (optional): A suffix to add to working
#   database table names to distinguish different squeezes.
#
# - remove_if and available_flag (optional): If specified,
#   skims_to_sem will consider skims that satisfy remove_if
#   (as a Boolean SQL expression) to represent the mode
#   not being available.
#
# - luz_skim_names: The list of skim names that should be in
#   output file, in the order they should appear.
#
# - luz_skim_renames (optional): A mapping from internal
#   skim names to names in the output file. Only include skim
#   names that are different from the output file names.
#
# - luz_skim_aliases (optional): A mapping from internal
#   skim names to names in the output file. Unlike
#   with luz_skim_renames, both the original and renamed
#   skim will appear in the output file.
#
# - long_distance_logsums (optional): If provided,
#   skims_to_sem calculates non-linear logsums with
#   specified reference and cutoff values. See the
#   Alberta example for the format.
#
# - check_skim (optional): A skim to check for
#   impossibly large values.
#
# - externals: The list of zone numbers that
#   represent external zones.
squeezes: []
