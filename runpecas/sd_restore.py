import argparse

from hbautil import sd_backup_restore
import pecas_routines as pr


def main():
    parser = argparse.ArgumentParser(
        description="Loads the SD database from the filesystem."
    )
    parser.add_argument(
        "-o", "--overwrite", action="store_true", help=""
    )

    args = parser.parse_args()

    ps = pr.load_pecas_settings()
    pr.set_up_logging(ps)
    sd_backup_restore.restore_sd(
        ps,
        combine=True, overwrite=args.overwrite, use_environ=False, schema=ps.sd_schema, threaded=False
    )


if __name__ == "__main__":
    main()
