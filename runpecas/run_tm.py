
from __future__ import (
    absolute_import,
)

import logging
import time
from os.path import (
    join,
)

import pecas_routines as pr
from hba_tdm_client import tdmclient


class TMRunner(pr.TMRunner):
    tdm_wait = 300

    def run_tm(self, ps, year):
        client = tdmclient.TdmClient(
            year=year,
            scenario=ps.scenario,
            server_url=ps.tm_server_url,
        )
        logging.info("Uploading travel model input files")
        logging.info("Uploading employment")
        client.upload_employment(join(ps.scendir, str(year), "Employment.csv"))
        logging.info("Uploading population")
        client.upload_population(join(ps.scendir, str(year), "Outputsamples_income.csv"))
        logging.info("Uploading TAZ trips")
        client.upload_taztrips(join(ps.scendir, str(year), "tazTrips.csv"))

        while not self.tm_is_available(ps):
            logging.info("TDM is busy, waiting {:.1f} minutes".format(TMRunner.tdm_wait / 60))
            time.sleep(TMRunner.tdm_wait)

        client.start_tdm_script(ps.tm_script_name)
        try:
            client.wait_for_tdm()
        except tdmclient.TravelModelFailure:
            logging.fatal("TM model did not run successfully in year {}".format(year))
            raise

    def retrieve_skims(self, ps, year):
        client = tdmclient.TdmClient(
            year=year,
            scenario=ps.scenario,
            server_url=ps.tm_server_url,
        )
        logging.info("Downloading skims from travel model")
        client.download_skimfile(join(
            ps.scendir, str(year), ps.taz_skims_fname.format(year=year, scenario=ps.scenario) + ".csv"
        ))

    # noinspection PyMethodMayBeStatic
    def tm_is_available(self, ps):
        client = tdmclient.TdmClient(server_url=ps.tm_server_url)
        status = client.tdm_status()
        return not status["tdm_is_lock"]

    def tm_precheck(self, ps):
        tm_years = [year for year in ps.tmyears if year >= ps.tm_startyear]
        scenario = ps.scenario
        client = tdmclient.TdmClient(server_url=ps.tm_server_url)
        return client.tdm_precheck(scenario, tm_years)
