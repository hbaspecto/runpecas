from os.path import join

import sys

from hbautil import scriptutil as su
from runpecas import update_settings, machine_settings


def main():
    ve_dir = sys.argv[1]

    runpecas_base_dir = join(ve_dir, "src", "runpecas")
    runpecas_dir = join(runpecas_base_dir, "runpecas")
    properties_dir = join("AllYears", "Inputs")
    runpecas_properties_dir = join(runpecas_base_dir, "scenario", properties_dir)

    update_settings.main(
        template_fname=join(runpecas_dir, "pecas.yml"),
        user_fname="pecas.yml",
        python_fname="pecas_settings.py",
    )
    update_settings.main(
        template_fname=join(runpecas_dir, "machine.yml"),
        user_fname="machine.yml",
        python_fname="machine_settings.py",
    )
    update_settings.main(
        template_fname=join(runpecas_dir, "mrsgui-config.yml"),
        user_fname="mrsgui-config.yml",
    )
    su.copy_with_backup(join(runpecas_properties_dir, "aa.properties"), join(properties_dir, "aa.properties"))
    su.copy_with_backup(join(runpecas_properties_dir, "sd.properties"), join(properties_dir, "sd.properties"))
    su.copy_no_clobber(join(runpecas_dir, "project_code_template.py"), "project_code.py")
    su.copy_no_clobber(join(runpecas_dir, "skim-squeezing.yml"), "skim-squeezing.yml")
    machine_settings.main()
