#!/bin/bash
set -x #Echo on

# Remove big AA output files
find . -type f \( \
  -name FloorspaceZoneTotalMakeUse.bin \
  -or -name TechnologyChoice.csv \
  -or -name ZonalMakeUse.csv \
  -or -name flow_matrices.omx \
  -or -name TAZDetailedUse.csv \
  -or -name TAZDetailedMake.csv \
  -or -name "buying_*.zipMatrix" \
  -or -name "selling_*.zipMatrix" \
  \) -delete
  
# Subversion sometimes keeps too much in the pristine directories
svn cleanup