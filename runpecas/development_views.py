import pecas_routines as pr


def main(ps):
    q = pr.sd_querier(ps)

    q.query("drop view if exists {sch}.latest_zoning_code cascade;")
    q.query(
        "create view {sch}.latest_zoning_code as\n"
        "select last_zoning.pecas_parcel_num,\n"
        "coalesce(pp.zoning_rules_code, last_zoning.zoning_rules_code) as zoning_rules_code\n"
        "from (select * from {sch}.phasing_plan_xref) pp\n"
        "right join\n"
        "(\n"
        "   select distinct on (pzx.pecas_parcel_num)\n"
        "   pzx.pecas_parcel_num, pzx.zoning_rules_code\n"
        "   from {sch}.parcel_zoning_xref pzx\n"
        "   order by pecas_parcel_num, year_effective desc\n"
        ") last_zoning\n"
        "on pp.pecas_parcel_num = last_zoning.pecas_parcel_num;"
    )

    q.query("drop view if exists {sch}.max_far_by_space_type_id;")
    q.query(
        "create or replace view {sch}.max_far_by_space_type_id as\n"
        "select pecas_parcel_num, 'ST' || space_type_id as allowed_space_type, max_intensity_permitted\n"
        "from {sch}.latest_zoning_code lzc\n"
        "join {sch}.zoning_permissions zp\n"
        "using (zoning_rules_code)\n"
        "join {sch}.space_types_i sti\n"
        "using (space_type_id)\n"
        "where pecas_parcel_num is not null;"
    )

    crosstab_query = q.query(
        "select get_crosstab_statement(\n"
        "'{sch}', 'max_far_by_space_type_id',\n"
        "ARRAY['pecas_parcel_num'], 'allowed_space_type', 'max(max_intensity_permitted)');"
    )[0][0][:-1]  # We need the [:-1] to drop the extra semicolon

    q.query("drop materialized view if exists {sch}.allowed_and_max_far;")
    q.query(
        "create materialized view {sch}.allowed_and_max_far as\n"
        "select pbwg.pecas_parcel_num as pecas_parcel_num_p,\n"
        "pbwg.geom, pbwg.land_area, mostintense.highest_intensity_space_type_id,\n"
        "mostintense.highest_intensity_quantity, mostintense.most_intense_far,\n"
        "mostintense.most_intense_year, xtab_maxs.*\n"
        "from\n"
        "(\n"
        "   select pecas_parcel_num, geom, land_area from {sch}.parcels_backup_with_geom\n"
        ") pbwg\n"
        "join\n"
        "(\n"
        "   (\n"
        "       select distinct on (original_pecas_parcel_num)\n"
        "       original_pecas_parcel_num as pecas_parcel_num,\n"
        "       new_space_type_id as highest_intensity_space_type_id,\n"
        "       new_space_quantity as highest_intensity_quantity,\n"
        "       new_space_quantity / land_area as most_intense_far,\n"
        "       year_run as most_intense_year from {sch}.development_events_history\n"
        "       where event_type in ('C', 'CS', 'A', 'AS')\n"
        "       order by original_pecas_parcel_num, new_space_quantity desc\n"
        "   ) mostintense\n"
        "   right join\n"
        "   ("
        + crosstab_query +
        "   ) xtab_maxs\n"
        "   using (pecas_parcel_num)\n"
        ")\n"
        "using (pecas_parcel_num);"
    )


if __name__ == "__main__":
    ps = pr.load_pecas_settings()
    main(ps)
