import logging
from os.path import join

import hbautil.scriptutil as su
import pecas_routines as pr
import runpecas.input_output_views as views
from hbautil import settings


def main(ps):
    q = pr.sd_querier(ps)
    latest_run_year = q.query_scalar(
        "select * from {sch}.current_year_table"
    )

    vs = settings.from_yaml(join(ps.scendir, "views.yml"))
    views.create_space_categories(vs, q)

    creator = ViewCreator()

    creator.create(q, "Base year space", "base_parcels_uneditable_with_geom")
    creator.create(q, "Most intense development", "most_intense_development")
    creator.create(q, "First development", "first_development")
    with q:
        space_type_names = q.query(
            "select distinct aa_commodity from {sch}.space_to_commodity order by aa_commodity"
        )
        space_type_columns = ",\n".join([f"\"{st_name[0]}\" double precision" for st_name in space_type_names])
    for year in ps.snapshotyears:
        if year <= latest_run_year:
            creator.create(q, "Snapshots", f"parcels_{year}_with_geom", year=year)
            creator.create(
                q, "Floorspace", f"floorspacei_{year}_with_geom",
                year=year, source_table=f"parcels_{year}", space_type_columns=space_type_columns
            )
    creator.create(
        q, "Floorspace", f"floorspacei_{ps.baseyear}_with_geom",
        year=ps.baseyear, source_table="parcels_backup_with_geom", space_type_columns=space_type_columns
    )


class ViewCreator(views.ViewCreator):
    def query_external(self, tr, mark, **kwargs):
        logging.info("Creating " + mark)
        tr.query_external(
            su.in_this_directory(__file__, "output_views.sql"),
            start="START " + mark,
            end="END " + mark,
            **kwargs
        )


def run_default():
    main_ps = pr.load_pecas_settings()
    pr.set_up_logging(main_ps)
    main(main_ps)


if __name__ == "__main__":
    run_default()
