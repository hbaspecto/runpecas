for D in $(find . -mindepth 1 -maxdepth 1 -type d) ; do
    pushd $D ;
    cp ExchangeResults.csv ExchangeResultsI.csv
    popd
done