import logging
from os.path import join

import hbautil.scriptutil as su
import pecas_routines as pr
import runpecas.input_output_views as views
from hbautil import settings


def main(ps):
    q = pr.sd_querier(ps)
    vs = settings.from_yaml(join(ps.scendir, "views.yml"))
    views.create_space_categories(vs, q)

    creator = ViewCreator()

    creator.create(q, "Zoning category type", "zoning_category_type")
    creator.create(q, "Base year space", "base_parcels_with_geom")
    creator.create(q, "Earliest Zoning", "earliest_zoning")
    creator.create(q, "All Zoning", "zoning_xref_geom")
    creator.create(q, "All Phasing", "phasing_plan_xref_geom")
    creator.create(q, "Costs", "base_costs", baseyear=ps.baseyear)
    creator.create(q, "Fees", "base_fees", baseyear=ps.baseyear, amortization=ps.amortization_factor)
    creator.create(q, "Local effects", "base_local_effect_distances", baseyear=ps.baseyear)
    creator.create(q, "Site spec", "sitespec_parcels_with_geom")


class ViewCreator(views.ViewCreator):
    def query_external(self, tr, mark, **kwargs):
        logging.info("Creating " + mark)
        tr.query_external(
            su.in_this_directory(__file__, "input_views.sql"),
            start="START " + mark,
            end="END " + mark,
            **kwargs
        )


def run_default():
    main_ps = pr.load_pecas_settings()
    pr.set_up_logging(main_ps)
    main(main_ps)


if __name__ == "__main__":
    run_default()
