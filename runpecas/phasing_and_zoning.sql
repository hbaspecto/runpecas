-- View: i268_with_input_views.earliest_zoning

DROP MATERIALIZED VIEW i268_with_input_views.phasing_and_zoning;

CREATE MATERIALIZED VIEW i268_with_input_views.phasing_and_zoning
AS
 SELECT z_and_p.*, zp.space_category, zp.max_intensity_permitted
 from 
	( -- this unions all the zoning and phasing entries for the parcel 
			select par.*, zx_1.zoning_rules_code, zx_1.year_effective as release_year,
		  null::integer as plan_id, null::integer as phase_number
		 from i268_with_input_views.parcels_backup_with_geom par 
		 left join i268_with_input_views.parcel_zoning_xref zx_1 using (pecas_parcel_num) 
		 union all
		 select parp.*, ppx.zoning_rules_code, ppl.initial_release_year as release_year,
		 ppx.plan_id, ppx.phase_number from i268_with_input_views.parcels_backup_with_geom parp 
		 left join i268_with_input_views.phasing_plan_xref ppx using (pecas_parcel_num)
		 left join i268_with_input_views.phasing_plans ppl using (plan_id)
	) z_and_p
	JOIN ( -- this classifies the zoning code based on the maximum intensity permitted, for simplified zoning
		-- TODO: We should probably have an array_agg to list out all the allowed types
		SELECT DISTINCT ON (sub.zoning_rules_code) sub.zoning_rules_code,
                    sub.space_category,
                    sub.max_intensity_permitted
                   FROM ( SELECT zp_1.zoning_rules_code,
                            zp_1.space_type_id,
                            sc.space_category,
                            zp_1.max_intensity_permitted
                           FROM i268_with_input_views.zoning_permissions zp_1
                             JOIN i268_with_input_views.space_categories sc USING (space_type_id)) sub
                  ORDER BY sub.zoning_rules_code, sub.max_intensity_permitted DESC, sub.space_type_id) zp 
	USING (zoning_rules_code)
	order by pecas_parcel_num, release_year;


CREATE INDEX phasing_and_zoning_geom_idx
    ON i268_with_input_views.phasing_and_zoning USING gist
    (geom)
    TABLESPACE pg_default;
	
select * from i268_with_input_views.phasing_and_zoning where pecas_parcel_num =9802801173073501;

CREATE UNIQUE INDEX phasing_and_zoning_parcel_year_idx
    ON i268_with_input_views.phasing_and_zoning USING btree
    (pecas_parcel_num, release_year, plan_id)
    TABLESPACE pg_default;