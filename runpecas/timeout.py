"""
Provides a variant of subprocess.call() that kills the process if it takes too long.
"""
import subprocess


def call(*args, **kwargs):
    """
    Calls a process, killing it if it takes too long.
    
    The signature is the same as subprocess.call(), except that an extra keyword argument timeout can be provided,
    which represents the time in seconds to wait for the process to finish.
    """
    timeout_key = "timeout"
    if timeout_key in kwargs:
        limit = kwargs[timeout_key]
        del kwargs[timeout_key]
        proc = subprocess.Popen(*args, **kwargs)
        try:
            retcode = proc.wait(limit)
        except subprocess.TimeoutExpired:
            proc.kill()
            retcode = 1
        return retcode
    else:
        return subprocess.call(*args, **kwargs)


def timeout(proc):
    if proc.poll() is None:
        proc.kill()
