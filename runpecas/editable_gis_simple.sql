
create view i272.phasing_plan_xref_geom
as SELECT pecas_parcel_num, plan_id, phase_number, zoning_rules_code, geom
	FROM i272.phasing_plan_xref
	join i272.parcels_backup_with_geom using (pecas_parcel_num);

CREATE OR REPLACE FUNCTION i272.update_phasing()
  RETURNS TRIGGER 
  LANGUAGE PLPGSQL
  AS
$$
BEGIN
	-- TODO check to make sure geometry hasn't changed
	update i272.phasing_plan_xref set 
		plan_id = NEW.plan_id, 
		phase_number = NEW.phase_number,
		zoning_rules_code = NEW.zoning_rules_code
	where pecas_parcel_num = OLD.pecas_parcel_num;
	RETURN NEW;
END;
$$
;

CREATE TRIGGER update_phasing
    INSTEAD OF update ON i272.phasing_plan_xref_geom
    FOR EACH ROW
    EXECUTE PROCEDURE i272.update_phasing();
	
drop view if exists i272.zoning_xref_geom;
create view i272.zoning_xref_geom
as SELECT pecas_parcel_num, zoning_rules_code, year_effective, geom
	FROM i272.parcel_zoning_xref
	join i272.parcels_backup_with_geom using (pecas_parcel_num);

CREATE OR REPLACE FUNCTION i272.update_zoning()
  RETURNS TRIGGER 
  LANGUAGE PLPGSQL
  AS
$$
BEGIN
	-- TODO check to make sure geometry hasn't changed
	update i272.parcel_zoning_xref set 
		year_effective = NEW.year_effective, 
		zoning_rules_code = NEW.zoning_rules_code
	where pecas_parcel_num = OLD.pecas_parcel_num;
	RETURN NEW;
END;
$$
;

CREATE TRIGGER update_zoning
    INSTEAD OF update ON i272.zoning_xref_geom
    FOR EACH ROW
    EXECUTE PROCEDURE i272.update_zoning();
	
	
CREATE MATERIALIZED VIEW i272.parcel_ratios
TABLESPACE pg_default
AS
 SELECT parcels_backup_with_geom.pecas_parcel_num,
    parcels_backup_with_geom.land_area / st_area(parcels_backup_with_geom.geom) AS land_ratio
   FROM i272.parcels_backup_with_geom
  WHERE st_area(parcels_backup_with_geom.geom) > 10::double precision
WITH DATA;


CREATE UNIQUE INDEX parcel_ratios_id_idx
    ON i272.parcel_ratios USING btree
    (pecas_parcel_num)
    TABLESPACE pg_default;
	
CREATE OR REPLACE VIEW i272.parcel_odd_ratios_with_geom
 AS
 SELECT parcel_ratios.pecas_parcel_num,
    parcel_ratios.land_ratio,
    parcels_backup_with_geom.parcel_id,
    parcels_backup_with_geom.year_built,
    parcels_backup_with_geom.taz,
    parcels_backup_with_geom.space_type_id,
    parcels_backup_with_geom.space_quantity,
    parcels_backup_with_geom.land_area,
    parcels_backup_with_geom.available_services_code,
    parcels_backup_with_geom.is_derelict,
    parcels_backup_with_geom.is_brownfield,
    parcels_backup_with_geom.geom
   FROM i272.parcel_ratios
     JOIN i272.parcels_backup_with_geom USING (pecas_parcel_num)
  WHERE parcel_ratios.land_ratio > 1.05::double precision OR parcel_ratios.land_ratio < 0.95::double precision;
  
CREATE OR REPLACE VIEW i272.phasing_zoning_both
 AS
 SELECT a.zoning_rules_code AS fz_zoning,
    a.year_effective,
    b.pecas_parcel_num,
    b.plan_id,
    b.phase_number,
    b.zoning_rules_code,
    c.geom
   FROM i272.parcel_zoning_xref a
     JOIN i272.phasing_plan_xref b USING (pecas_parcel_num)
     JOIN i272.parcels_backup_with_geom c USING (pecas_parcel_num)
  WHERE a.year_effective <> 2011;
