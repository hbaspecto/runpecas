import abc

import psycopg2 as pg

from pecas_routines.migrate import skip


def create_space_categories(vs, q):
    table_data = []
    for space_category in vs.space_categories:
        category_name = space_category["name"]
        for space_type in space_category["types"]:
            table_data.append([space_type, category_name])

    with q.transaction() as tr:
        try:
            tr.query(
                "create table {sch}.space_categories (\n"
                "   space_type_id integer primary key,\n"
                "   space_category varchar\n"
                ")"
            )
            tr.query_many("insert into {sch}.space_categories values (%s, %s)", table_data)
        except pg.ProgrammingError as e:
            skip(e, "space_categories")


class ViewCreator(metaclass=abc.ABCMeta):
    def create(self, q, mark, table_name, **kwargs):
        print(f"Creating {table_name}")
        with q.transaction() as tr:
            try:
                self.query_external(tr, mark, **kwargs)
            except pg.ProgrammingError as e:
                skip(e, table_name)

    @abc.abstractmethod
    def query_external(self, tr, mark, **kwargs):
        pass
