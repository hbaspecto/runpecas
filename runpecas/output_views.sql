-- START Base year space

drop  view if exists {sch}.base_parcels_uneditable_with_geom;

create view {sch}.base_parcels_uneditable_with_geom as
select par.pecas_parcel_num, par.space_type_id, st.space_type_name,
par.space_quantity, par.land_area, par.geom
from {sch}.parcels_backup_with_geom par
join {sch}.space_types_i st using (space_type_id);

-- END Base year space

-- START Most intense development

drop materialized view if exists {sch}.most_intense_development;

create materialized view {sch}.most_intense_development as
select distinct on (pecas_parcel_num)
par.pecas_parcel_num, deh.new_space_type_id as space_type_id, st.space_type_name,
deh.new_space_quantity as space_quantity, deh.year_run as year_built, par.geom
from {sch}.development_events_history deh
join {sch}.parcels_backup_with_geom par
on par.pecas_parcel_num = deh.original_pecas_parcel_num
join {sch}.space_types_i st
on st.space_type_id = deh.new_space_type_id
where event_type in ('C', 'A')
order by par.pecas_parcel_num, deh.new_space_quantity desc, deh.year_run;

create unique index on {sch}.most_intense_development (pecas_parcel_num);
create index on {sch}.most_intense_development using gist(geom);

-- END Most intense development

-- START First development

drop materialized view if exists {sch}.first_development;

create materialized view {sch}.first_development as
select distinct on (pecas_parcel_num)
par.pecas_parcel_num, deh.new_space_type_id as space_type_id, st.space_type_name,
deh.new_space_quantity as space_quantity, deh.year_run as year_built, par.geom
from {sch}.development_events_history deh
join {sch}.parcels_backup_with_geom par
on par.pecas_parcel_num = deh.original_pecas_parcel_num
join {sch}.space_types_i st
on st.space_type_id = deh.new_space_type_id
where event_type in ('C', 'A')
order by par.pecas_parcel_num, deh.year_run, deh.new_space_quantity desc;

create unique index on {sch}.first_development (pecas_parcel_num);
create index on {sch}.first_development using gist(geom);

-- END First development

-- START Snapshots

-- Note we have to use double %% in here to fool python.  To run this manually, replace the double % with single %

CREATE OR REPLACE FUNCTION drop_any_type_of_view_if_exists(IN _schemaname text, IN _viewname text)
RETURNS VOID AS
$$
BEGIN
    RAISE LOG 'Looking for (materialized) view named %%', _viewname;
    IF EXISTS (SELECT matviewname from pg_matviews where schemaname = _schemaname and matviewname = _viewname) THEN
        RAISE NOTICE 'DROP MATERIALIZED VIEW %%', _viewname;
        EXECUTE 'DROP MATERIALIZED VIEW ' || _schemaname || '.' || quote_ident(_viewname);
    ELSEIF EXISTS (SELECT viewname from pg_views where schemaname = _schemaname and viewname = _viewname) THEN
        RAISE NOTICE 'DROP VIEW %%', _viewname;
        EXECUTE 'DROP VIEW ' || _schemaname || '.' || quote_ident(_viewname);
    ELSE
        RAISE NOTICE 'NO VIEW %% found', _viewname;
    END IF;
END;
$$ LANGUAGE plpgsql;

select drop_any_type_of_view_if_exists('{sch}', 'parcels_{year}_with_geom');

create view {sch}.parcels_{year}_with_geom as
select par.*, st.space_type_name,
pg.geom
from {sch}.parcels_{year} par
join {sch}.parcels_backup_with_geom pg using (pecas_parcel_num)
join {sch}.space_types_i st on st.space_type_id = par.space_type_id;

create unique index if not exists parcels_pecas_num_{year}_idx on {sch}.parcels_{year} (pecas_parcel_num);

-- END Snapshots

-- START Floorspace

drop materialized view if exists {sch}.floorspacei_{year}_with_geom;

create materialized view {sch}.floorspacei_{year}_with_geom as
select ct.*, tazs.geom
from
(
	select * from crosstab(
	$source$
		select tazs.taz, sc.commodity, coalesce(fl.quantity, 0) as quantity from
		(
			select taz_number as taz from {sch}.tazs
		) tazs
		cross join
		(
			select distinct aa_commodity as commodity from {sch}.space_to_commodity
		) sc
		left join
		(
			select par.taz,
			com.aa_commodity as commodity,
			sum(par.space_quantity * com.weight) as quantity
			from {sch}.{source_table} par
			join {sch}.space_types_i st using (space_type_id)
			join {sch}.space_to_commodity com using (space_type_id)
			group by par.taz, com.aa_commodity
		) fl
		using (taz, commodity)
		order by taz, commodity
	$source$
	) as ct(
		taz integer,
		{space_type_columns}
	)
) ct
join {sch}.tazs on tazs.taz_number = ct.taz;

create unique index on {sch}.floorspacei_{year}_with_geom (taz);
create index on {sch}.floorspacei_{year}_with_geom using gist(geom);

-- END Floorspace
