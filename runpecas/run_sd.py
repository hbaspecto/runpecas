import logging
import argparse
import os
from os.path import join

import pecas_routines as pr
import hbautil.mapit_files as mf
from runpecas import timeout

from hbautil.classpath import build_classpath_in_path, build_classpath, vm_properties


# noinspection PyMethodMayBeStatic
class StandardSDRunner(pr.SDRunner):
    def run_sd(self, ps, year, skimfile=None, skimyear=None):
        prevyear = year - 1
        logging.debug("prevyear =" + str(prevyear))

        self.apply_price_smoothing(ps, year, skimfile, skimyear)

        if ps.apply_sitespec:
            pr.apply_site_spec(ps, year)

        if ps.update_space_limits:
            pr.update_space_limits(ps, year)

        retcode = -1

        for i in range(ps.get('sd_retries', 7)):
            args, kwargs = self.make_sd_program_call(ps, year)
            retcode = timeout.call(*args, timeout=ps.sd_retry_time * 3600, **kwargs)
            if retcode == 0:
                break
            else:
                pr.copy_program_log_file_into_year(ps, "sd", year, suffix=f"failed-{i}")

        pr.log_results_from_external_program(
            "SD model finished for year {}".format(year),
            "SD model did not run successfully in year {}".format(year),
            (retcode,)
        )

        pr.copy_program_log_file_into_year(ps, "sd", year)

        if pr.try_move_replace(
            join(ps.scendir, "AllYears", "Outputs", "developmentEvents.csv"),
            join(ps.scendir, str(year), "developmentEvents.csv"),
            "No development events to copy"
        ):
            pr.insert_development_events_into_history(ps, year)

        pr.try_move_replace(
            join(ps.scendir, "AllYears", "Outputs", "choiceUtilities.csv"),
            join(ps.scendir, str(year), "choiceUtilities.csv"),
            "No choice utilities to copy"
        )

        try:
            pr.copy_floorspace_summary(ps, year + 1)
        except IOError:
            logging.warning("No floorspace summary to copy")

        if ps.load_output_to_mapit:
            if ps.use_matchmaker:
                # Prepare Gale-Shapley control totals
                logging.info("Preparing Gale-Shapley Mapit upload")
                pr.prepare_gale_shapley_outputs(ps, year)

            pr.load_outputs_for_year(ps, year, mf.EAGER_SD)

    def make_sd_program_call(self, ps, year):
        """
        Returns the arguments that should be passed to subprocess.call to run the SD module, as a pair
        containing the positional arguments as a list and the keyword arguments as a dictionary.
        """
        classpath = build_classpath_in_path(
            join(ps.scendir, ps.codepath),
            specific_jars=[
                ps.pecasjar, ps.commonbasejar, ps.simpleormjar
            ],
            other_jars=[
                "or", "mtj", "postgresql",
                "log4j-1.2-api",
                "log4j-api", "log4j-core",
                "guava",
            ],
        )

        logging.info("classpath is " + classpath)

        logging.info("Starting PECAS SD Model run for year " + str(year))
        relyear = year - ps.baseyear

        vm_args = [
            "-Xmx" + ps.sd_memory,
            "-cp", build_classpath(join(ps.scendir, "AllYears", "Inputs"), classpath)
        ]
        self.add_properties(ps, vm_args)

        if ps.profiling:
            vm_args.append("-Xrunhprof:cpu=samples,file=hprof.txt,depth=24,thread=y")

        logging.info("running SD model for " + str(year) + " with " + " ".join(vm_args))
        return [
            [ps.javaRunCommand] +
            vm_args +
            ["com.hbaspecto.pecas.sd.StandardSDModel",
             str(ps.baseyear), str(relyear)]
        ], {}

    def add_properties(self, ps, vm_args):
        properties = dict(
            SCENDIR=ps.scendir,
            host=ps.sd_host,
            database=ps.sd_database,
            port=ps.sd_port,
            databaseuser=ps.sd_user,
            PGPASSWORD=ps.sd_password,
            SD_SCHEMA=ps.sd_schema,
            PARCELS=ps.parcels_table,
            CONSCTRL=ps.construction_control,
            IGNORE=ps.sd_ignore_errors,
            MINPARCEL=ps.min_parcel_size,
            MAXPARCEL=("Infinity" if ps.max_parcel_size is None else ps.max_parcel_size),
            AMORT=ps.amortization_factor,
            READXRES=(ps.read_exchange_results_in_sd or ps.smooth_prices_in_sd)
                      and not ps.apply_price_smoothing_outside_of_sd,
            SMOOTH=(ps.apply_price_smoothing_in_sd and not ps.apply_price_smoothing_outside_of_sd),
            GRAVITY=ps.gravity_exponent,
            DISTCOL=ps.distance_column,
            TAZLIMITS=ps.space_limits,
            MATCHMAKER=ps.use_matchmaker,
            MMALLOC=ps.matchmaker_allocation_level,
            MMCTRL=ps.matchmaker_control_total_level,
            FIXEDRANDOM=(ps.predefined_random_numbers and not ps.use_matchmaker),
            RANDOMSEED=ps.predefined_random_number_seed,
        )

        vm_args.extend(vm_properties(properties))

    def apply_price_smoothing(self, ps, year, skimfile, skimyear):
        if ps.apply_price_smoothing_outside_of_sd:
            command = os.path.join(ps.pgpath, "psql")
            pr.load_distances(ps, skimfile, skimyear)
            pr.load_exchange_results(ps, year)
            pr.smooth_prices(ps)
            pr.write_smoothed_prices(ps, year, command)

def get_args_and_run():
    parser = argparse.ArgumentParser(
        description="Runs the PECAS SD module for one year."
    )
    parser.add_argument("year", type=int, help="The year to run AA for.")
    parser.add_argument(
        "-s", "--settings", default="pecas.yml", help="The YAML file that PECAS should get its settings from")

    args = parser.parse_args()

    ps = pr.load_pecas_settings(args.settings)
    pr.set_up_logging(ps)
    _year = args.year
    sd = StandardSDRunner()
    sd.run_sd(ps,_year)

if __name__ == "__main__":
    get_args_and_run()
