# Code specific to this project. Fill in any function implementations you need, leaving the rest as "pass".


# Called at the start of the run, after any resetting.
def before_run(ps):
    pass


# Called at the start of each model year, before any modules have run in that year.
def start_of_year(ps, year):
    pass


# Called after the AA module finishes.
def after_aa(ps, year):
    pass


# Called after the ED module finishes.
def after_ed(ps, year):
    pass


# Called after the TM module finishes.
def after_tm(ps, year):
    pass


# Called after the SD module finishes.
def after_sd(ps, year):
    pass
