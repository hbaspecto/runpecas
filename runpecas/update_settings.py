import argparse
import csv
import os
from os.path import join

from hbautil import settings
import hbautil.scriptutil as su
import pecas_routines as pr


def main(template_fname, user_fname, python_fname=None):
    template = settings.template_from_yaml(template_fname, pattern_lists=pr.pecas_settings_pattern_lists)

    old_settings = None
    try:
        su.backup(user_fname, "backup")
        old_settings = settings.from_yaml(user_fname, pattern_lists=pr.pecas_settings_pattern_lists)
        print("Loaded existing settings from {}".format(user_fname))
    except IOError:
        if python_fname is None:
            print("Existing settings file {} not found, creating from template".format(user_fname))
        else:
            print("Existing settings file {} not found".format(user_fname))
            try:
                su.backup(python_fname, "backup")
                old_settings = settings.from_python(python_fname)
                os.remove(python_fname)
                print("Loaded existing settings from {}".format(python_fname))
            except IOError:
                print("Existing settings file {} not found, creating {} from template".format(python_fname, user_fname))

    if old_settings is None:
        new_settings = template
    else:
        # noinspection PyUnboundLocalVariable
        new_settings = template.update(old_settings)
        if hasattr(old_settings, "maximum_sd_prices") and old_settings.maximum_sd_prices:
            _extract_maximum_sd_prices(old_settings)

    settings.template_to_yaml(user_fname, new_settings)


def _extract_maximum_sd_prices(old_settings):
    with open(join(old_settings.scendir, "AllYears", "Inputs", "SDPriceCaps.csv"), "w", newline="") as f:
        writer = csv.writer(f)
        writer.writerow(["AASpaceType", "MaximumPrice"])
        for space_type, max_price in old_settings.maximum_sd_prices.items():
            writer.writerow([space_type, max_price])


def parse_arguments_and_run():
    parser = argparse.ArgumentParser(
        description="Updates the YAML configuration files to the latest template."
    )
    parser.add_argument("template", help="The template file.")
    parser.add_argument("user_file", help="The name of the current configuration file.")
    parser.add_argument(
        "-p", "--old-python", help="The old-style Python settings file to look for if user_file isn't found."
    )

    args = parser.parse_args()

    main(args.template, args.user_file, args.old_python)


if __name__ == "__main__":
    parse_arguments_and_run()
