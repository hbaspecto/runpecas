import pecas_routines as pr


def main():
    print ("This should build the standard SD views")
    
    '''
    Step 1: create some views
    Step 2: generate some crosstab queries
    Step 3: generate some more views and materialized views using the crosstab queries
    Step 4: Copy a templated .qgs project and replace the table names with the ones we just created
    '''

if __name__ == "__main__":
    ps = pr.load_pecas_settings()
    pr.set_up_logging(ps)
    main()
