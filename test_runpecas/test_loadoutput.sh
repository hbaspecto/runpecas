## I Don't know how to write test cases for shell commands ##

python3 ../../PECAS\ run\scripts/loadOutput.py

## test should return help 

python3 ../../PECAS\ run\scripts/loadOutput.py 0,2 2011-2025

## Should try to load sequence 0 and 2 for 2011 through 2025 to Mapit. 

python3 ../../PECAS\ run\scripts/loadOutput.py -c 0,2 2011-2025

## Should try to remove pecas output for the scenario, then load sequence 0 and 2 for 2011 through 2025

python3 ../../PECAS\ run\scripts/loadOutput.py -c -i http 0,2 2011-2025

## Should do the same but should force the http interface instead of the sql interface

## These tests all need test data and need to be configured to work through a mapit that is staging not production.

