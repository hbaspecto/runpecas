import unittest

from hbautil import settings
from runpecas import run_aa


class TestRunAA(unittest.TestCase):
    def test_year_since_last_run(self):
        ysl = run_aa._year_since_last_run
        ps = settings.Settings(aayears=[2011, 2013, 2014, 2016, 2020, 2021])
        self.assertEqual(0, ysl(ps, 2011))
        self.assertEqual(1, ysl(ps, 2012))
        self.assertEqual(2, ysl(ps, 2013))
        self.assertEqual(1, ysl(ps, 2014))
        self.assertEqual(4, ysl(ps, 2020))
        self.assertEqual(1, ysl(ps, 2021))
        self.assertEqual(16, ysl(ps, 2037))
