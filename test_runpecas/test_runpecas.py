import glob
import logging
import os
import shutil
import time
from os.path import join
import unittest

import pecas_routines as pr
from pecas_test.testutil import recreate_test_folder
from runpecas.runpecas import main as run, PrecheckFailure, pre_check
import runpecas.run_aa as aa
import runpecas.run_sd as sd
from pecas_test import mockscen
from pecas_test import mocksettings
import hbautil.scriptutil as su
import test_runpecas.machine_settings_for_testing as ms

dummy_command = [ms.python_command, "-c", "pass"]

gen_ps = mocksettings.PecasSettings().with_machine_settings().with_test_sd().clone_with(
    scendir=join("test", "Test runs"),
    scenario="T01",
    skim_fname="Skims.csv",
    sd_retry_time=2 / 3600,
    aa_max_iterations=100,
    tmyears=[],

    # All optional features are turned off by default!
    load_output_to_mapit=False,
    generate_cityphi=False,
    travel_model_input_years=[],
    employment=False,
    resume_run=False,
    reset_initial_database=False,
    adaptive_phasing=False,
    scenario_base=None,
    scenario_ed_base=None,
    scenario_ed_inputs=None,
    ed_from_files=False,
    squeeze_skims=False,
    scale_technology_options=False,
    use_aa_to_sd_price_ratio=False,
    allocate_am_totals=False,
    labour_make_use=False,
    find_expected_values_years=[],
    snapshotyears=[],
    apply_price_smoothing_outside_of_sd=False,
    apply_sitespec=False,
    update_space_limits=False,
    use_tm=False,
    floorspace_calc_delta=False,
    calculate_import_export_size=False,
    allocate_tm_totals=False,
)


class TestPrecheck(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.ps = (
            gen_ps.with_scenario_directory(join("test", "prechecks")).
                between_years(2010, 2030)
        )

    def testPrecheckSucceeds(self):
        self.pre_check(self.ps)

    def testPrecheckFailsIfNoAAInTMYear(self):
        ps = (
            self.ps.
                travel_model_in(2016, 2020, 2026).
                aa_only_in_odd_years()
        )
        with self.assertRaises(PrecheckFailure):
            self.pre_check(ps)
        # But if we turn the travel model off, the precheck should pass.
        self.pre_check(ps.clone_with(use_tm=False))

    @staticmethod
    def pre_check(ps):
        pre_check(ps, aa_runner=MockAA([]), sd_runner=MockSD([]), tm_runner=MockTM([]))


class TestRunPecas(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        mockscen.create_database(gen_ps)

    def setUp(self):
        self.standard_ps = gen_ps
        self.log = []
        mockscen.set_up_tables(self.standard_ps)
        self._recreate_year_folders(gen_ps, su.irange(2019, 2028))

    def testSimpleOneYearRun(self):
        _ps = self.standard_ps.between_years(2019, 2019)
        run(_ps, aa_runner=MockAA(self.log), sd_runner=MockSD(self.log))
        self.assertEqual(["2019AA"], self.log)

    def testSimpleFiveYearRun(self):
        _ps = self.standard_ps.between_years(2019, 2023)
        run(_ps, aa_runner=MockAA(self.log), sd_runner=MockSD(self.log))
        self.assertEqual(
            ["2019AA", "2019SD", "2020AA", "2020SD",
             "2021AA", "2021SD", "2022AA", "2022SD", "2023AA"],
            self.log
        )

    def testRetryOnSDCrash(self):
        _ps = self.standard_ps.between_years(2019, 2020)
        run(_ps, aa_runner=MockAA(self.log), sd_runner=CrashOnceSD(self.log))
        self.assertEqual(["2019AA", "2019SD(crashed)", "2019SD", "2020AA"], self.log)
        self.assertTimestampedFileExistsInDirectory(join(_ps.scendir, "2019"), "sd-T01-{}-failed-0.log")
        self.assertTimestampedFileExistsInDirectory(join(_ps.scendir, "2019"), "sd-T01-{}.log")

    def testRetryOnSDHang(self):
        _ps = self.standard_ps.between_years(2019, 2020)
        run(_ps, aa_runner=MockAA(self.log), sd_runner=HangOnceSD(self.log))
        self.assertEqual(["2019AA", "2019SD(hung)", "2019SD", "2020AA"], self.log)
        self.assertTimestampedFileExistsInDirectory(join(_ps.scendir, "2019"), "sd-T01-{}-failed-0.log")
        self.assertTimestampedFileExistsInDirectory(join(_ps.scendir, "2019"), "sd-T01-{}.log")

    def testEmployment(self):
        _ps = self.standard_ps.between_years(2019, 2020).with_emp_pop()
        run(_ps, aa_runner=FastEmpPopAA(self.log), sd_runner=MockSD(self.log))
        self.assertEqual(["2019AA", "2019Emp", "2019SD", "2020AA", "2020Emp"], self.log)

    def testTravelModelIntegrationSlowEmp(self):
        _ps = self.standard_ps.between_years(2019, 2028).travel_model_in(2022, 2025).with_emp_pop()
        write_file(join(_ps.scendir, "2022", "PopSynBaseTargets.csv"), "Nothing here")
        write_file(join(_ps.scendir, "2025", "PopSynBaseTargets.csv"), "Nothing here")
        run(_ps,
            aa_runner=SlowEmpPopAA(self.log),
            sd_runner=MockSD(self.log),
            tm_runner=MockTM(self.log))
        self.assertEqual(
            ["2019AA", "2019Emp", "2019SD", "2020AA", "2020SD", "2021AA", "2021SD", "2022AA",
             "2020Emp", "2021Emp", "2022Emp", "2022Pop",
             "2022TM", "2022TMEnd",
             "2022SD", "2023AA", "2023Emp", "2023SD", "2024AA", "2024SD", "2025AA",
             "2024Emp", "2025Emp", "2025Pop",
             "2025TM", "2025TMEnd",
             "2025SD", "2026AA", "2026Emp", "2026SD", "2027AA", "2027SD", "2028AA",
             "2027Emp", "2028Emp"],
            self.log
        )

    def testTravelModelIntegrationFastEmp(self):
        _ps = self.standard_ps.between_years(2019, 2028).travel_model_in(2022, 2025).with_emp_pop()
        write_file(join(_ps.scendir, "2022", "PopSynBaseTargets.csv"), "Nothing here")
        write_file(join(_ps.scendir, "2025", "PopSynBaseTargets.csv"), "Nothing here")
        run(_ps,
            aa_runner=FastEmpPopAA(self.log),
            sd_runner=MockSD(self.log),
            tm_runner=MockTM(self.log))
        self.assertEqual(
            ["2019AA", "2019Emp", "2019SD", "2020AA", "2020Emp", "2020SD",
             "2021AA", "2021Emp", "2021SD", "2022AA", "2022Emp", "2022Pop",
             "2022TM", "2022TMEnd",
             "2022SD", "2023AA", "2023Emp", "2023SD", "2024AA", "2024Emp", "2024SD",
             "2025AA", "2025Emp", "2025Pop",
             "2025TM", "2025TMEnd",
             "2025SD", "2026AA", "2026Emp", "2026SD", "2027AA", "2027Emp", "2027SD",
             "2028AA", "2028Emp"],
            self.log
        )

    def testLogFilesWithDetailedNames(self):
        sub_path = join(self.standard_ps.scendir, "Detailed log file names")
        _ps = self.standard_ps.between_years(2019, 2020).with_scenario_directory(sub_path)
        self._recreate_scenario_folder(_ps, [2019, 2020])
        pr.set_up_logging(_ps, "DetailedNames")
        logging.getLogger("DetailedNames").info("Testing is fun!")
        run(
            _ps,
            aa_runner=HollowAA(self.log),
            sd_runner=HollowSD(self.log),
        )
        self.assertTimestampedFileExistsInDirectory(_ps.scendir, "pecas-T01-{}.log")
        self.assertTimestampedFileExistsInDirectory(join(_ps.scendir, "2019"), "aa-T01-{}.log")
        self.assertTimestampedFileExistsInDirectory(join(_ps.scendir, "2019"), "sd-T01-{}.log")
        self.assertTimestampedFileExistsInDirectory(join(_ps.scendir, "2020"), "aa-T01-{}.log")

    def _recreate_scenario_folder(self, ps, years=None):
        try:
            shutil.rmtree(ps.scendir)
        except FileNotFoundError:
            pass
        os.makedirs(ps.scendir)
        if years is not None:
            self._recreate_year_folders(ps, years)

    def _recreate_year_folders(self, ps, years):
        for year in years:
            self._recreate_subfolder_in_scenario_folder(ps, str(year))

    @staticmethod
    def _recreate_subfolder_in_scenario_folder(ps, folder_name):
        recreate_test_folder(join(ps.scendir, folder_name))

    def assertTimestampedFileExistsInDirectory(self, directory, fname):
        pattern = fname.format("????????-????")
        self.assertEqual(1, len(glob.glob(join(directory, pattern))))


class MockAA(pr.AARunner):
    def __init__(self, log):
        self.log = log

    def run_aa(self, ps, year, skimfile, skimyear, dbyear=None, load=True):
        self.log.append(f"{year}AA")
        with open(join(ps.scendir, str(year), "ExchangeResults.csv"), "w") as f:
            f.write("placeholder")


class HollowAA(aa.StandardAARunner):
    """
    An AA missing the Java program call, for testing the Python parts of the
    model.
    """

    def __init__(self, log):
        self.log = log
        super().__init__()

    def make_aa_program_call(self, ps, year, skimyear, skimfile):
        self.log.append(f"{year}AA")
        write_file(join(ps.scendir, "event.log"), "Run succeeded!")
        with open(join(ps.scendir, str(year), "ExchangeResults.csv"), "w") as f:
            f.write("placeholder")
        return [dummy_command], {}


class EmpPopAA(HollowAA):
    def __init__(self, log):
        super().__init__(log)

    def run_aa(self, *args, **kwargs):
        super().run_aa(*args, **kwargs)
        time.sleep(0.01)

    def run_popsyn(self, ps, year):
        self.log.append(f"{year}Pop")


class SlowEmpPopAA(EmpPopAA):
    def run_employment(self, ps, year):
        self.log.append(f"{year}Emp")
        time.sleep(5)


class FastEmpPopAA(EmpPopAA):
    def run_employment(self, ps, year):
        self.log.append(f"{year}Emp")


class MockTM(pr.TMRunner):
    def __init__(self, log):
        self.log = log

    def run_tm(self, ps, year):
        self.log.append(f"{year}TM")
        self.log.append(f"{year}TMEnd")

    def retrieve_skims(self, ps, year):
        pass

    def tm_is_available(self, ps):
        return True

    def tm_precheck(self, ps):
        return None


class MockSD(pr.SDRunner):
    def __init__(self, log):
        self.log = log

    def run_sd(self, ps, year, skimfile=None, skimyear=None):
        self.log.append(f"{year}SD")


class HollowSD(sd.StandardSDRunner):
    """
    An SD missing the Java program call, for testing the Python parts of the
    model.
    """

    def __init__(self, log):
        self.log = log
        super().__init__()

    def make_sd_program_call(self, ps, year):
        self.log.append(f"{year}SD")
        write_file(join(ps.scendir, "event.log"), "Run succeeded!")
        return [dummy_command], {}


class CrashOnceSD(HollowSD):
    """
    Crashes the first time it's run, succeeds the second time, to test retrying
    """

    def __init__(self, log):
        self.crashed_already = False
        super().__init__(log)

    def make_sd_program_call(self, ps, year):
        if self.crashed_already:
            return super().make_sd_program_call(ps, year)
        else:
            self.log.append(f"{year}SD(crashed)")
            self.crashed_already = True
            write_file(join(ps.scendir, "event.log"), "Run crashed!")
            return [[ms.python_command, "-c", "import sys; sys.exit(1)"]], {}


class HangOnceSD(HollowSD):
    """
    Hangs indefinitely the first time it's run, succeeds the second time, to test retrying
    """

    def __init__(self, log):
        self.hung_already = False
        super().__init__(log)

    def make_sd_program_call(self, ps, year):
        if self.hung_already:
            return super().make_sd_program_call(ps, year)
        else:
            self.log.append(f"{year}SD(hung)")
            self.hung_already = True
            write_file(join(ps.scendir, "event.log"), "Run hanging...")
            return [[ms.python_command, "-c", "while True:\n\tpass"]], {}


def write_file(fname, contents):
    with open(fname, "w") as outf:
        outf.write(contents)
