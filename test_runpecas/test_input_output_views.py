import unittest
from os.path import join

import pecas_routines as pr
from runpecas import input_views, output_views
from pecas_test import mockscen, mocksettings

gen_ps = mocksettings.PecasSettings().with_test_sd().with_machine_settings()
alberta_ps = gen_ps.between_years(2011, 2050).clone_with(
    sd_schema="i240gts",
    scendir="test/input_output_views/alberta",
    amortization_factor=0.05,
    snapshotyears=[2020, 2030, 2040, 2050],
)
atlanta_ps = gen_ps.between_years(2015, 2045).clone_with(
    sd_schema="t119s",
    scendir="test/input_output_views/atlanta",
    amortization_factor=0.0823746504516875,
    snapshotyears=[2025, 2035, 2045],
)


class TestInputOutputViews(unittest.TestCase):
    """
    Tests generation of the standard input and output views.
    """

    @classmethod
    def setUpClass(cls):
        mockscen.create_database(gen_ps, geometry=True, crosstab=True)
        mockscen.pg_restore_plain(alberta_ps, join("test", "input_output_views", "alberta.sql"))
        mockscen.pg_restore_plain(atlanta_ps, join("test", "input_output_views", "atlanta.sql"))

    def testRestore(self):
        self.assertNonEmpty(alberta_ps, "parcels")
        self.assertNonEmpty(atlanta_ps, "parcels")

    def testInputViewsAlberta(self):
        self.runInputViewTest(alberta_ps, 24)

    def testOutputViewsAlberta(self):
        self.runOutputViewTest(alberta_ps, [2011, 2020, 2030, 2040, 2050])

    def testInputViewsAtlanta(self):
        self.runInputViewTest(atlanta_ps, 6)

    def testOutputViewsAtlanta(self):
        self.runOutputViewTest(atlanta_ps, [2015, 2025, 2035, 2045])

    def runInputViewTest(self, ps, num_space_types):
        input_views.main(ps)
        self.assertCountIs(ps, "space_categories", num_space_types)
        self.assertNonEmpty(ps, "base_parcels_with_geom")
        self.assertNonEmpty(ps, "earliest_zoning")
        self.assertNonEmpty(ps, "base_costs")
        self.assertNonEmpty(ps, "base_fees")
        self.assertNonEmpty(ps, "base_local_effect_distances")
        self.assertNonEmpty(ps, "sitespec_parcels_with_geom")
        # Test that we can run again!
        input_views.main(ps)

    def runOutputViewTest(self, ps, snapshot_years):
        output_views.main(ps)
        self.assertNonEmpty(ps, "most_intense_development")
        self.assertNonEmpty(ps, "first_development")
        for i, year in enumerate(snapshot_years):
            if i > 0:
                self.assertNonEmpty(ps, f"parcels_{year}_with_geom")
            self.assertNonEmpty(ps, f"floorspacei_{year}_with_geom")
        # Test that we can run again!
        output_views.main(ps)

    def assertNonEmpty(self, ps, table_name):
        with pr.sd_querier(ps) as q:
            self.assertGreater(
                q.query_scalar(
                    "select count(*) from {sch}.{table_name}",
                    table_name=table_name
                ),
                0
            )

    def assertCountIs(self, ps, table_name, expected):
        with pr.sd_querier(ps) as q:
            self.assertEqual(
                expected,
                q.query_scalar(
                    "select count(*) from {sch}.{table_name}",
                    table_name=table_name
                )
            )
